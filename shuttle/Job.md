class PackageJob:
    id -> buildid
    name -> name
    version -> version
    create_date -> create_date
    priority -> priority

    support_arches = ['amd64', 'i386']
    support_dists = ['xenial', 'arch', 'sid']

    xenial-amd64 - start_time
                   end_time
                   changed_time
                   status
                   log
