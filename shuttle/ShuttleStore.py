import os
import json
import threading
import uuid
from twisted.internet import threads

from ShuttleConfig import ShuttleConfig
import ShuttleJob
from ShuttleGit import GitBuilder
from ShuttleJob import JobStatus, UploadStatus
from ShuttleLog import ShuttleLog
import glob
from datetime import datetime

cfg = ShuttleConfig()
ShuttleLog()

lock = threading.Lock()

class PackageConfig():
    def __init__(self, pkgname, reponame='defaults'):
        _default = os.path.join(cfg.get('build','configdir'), 'package', reponame)
        if not os.path.exists(_default):
            _default = os.path.join(cfg.get('build','configdir'), 'package', 'defaults')
        default_config = os.path.join(_default, 'default.json')
        if os.path.exists(default_config):
            self.config = json.load(open(default_config))
        else:
            self.config = {}

        package_config = os.path.join(_default, '%s.json' % pkgname)
        if os.path.exists(package_config):
           self.config.update(json.load(open(package_config)))


class Repo():
    def __init__(self, name):
        self.name = name 
        self.configdir = os.path.join(cfg.get('build', 'configdir'), 'repo')
        self.json_config = os.path.join(self.configdir, '%s.json' % name)
        if not os.path.exists(self.json_config):
            raise IOError("config not exists")
        self.config = json.load(open(self.json_config))

    def packages(self, pkgs):
        result = []
        for pkg in pkgs:
            s_pkg = ShuttleJob.Package.selectBy(reponame=self.name, pkgname=pkg).orderBy("-id")
            if s_pkg.count():
                result.append(s_pkg[0])
            else:
                result.append({'pkgname': pkg, 'pkgver': '-', 'upload_status': '-'})
        return result

    @property
    def dists(self):
        return self.config.get('dists', [])

    def arches(self, dist=None):
        if dist is None:
            return self.config.get('arches', [])
        else:
            arches = self.config.get('%s-arches' % dist, None)
            if arches is not None:
                return arches
            else:
                return self.config.get('arches', [])

    @property
    def description(self):
        _description = "Description not filled in by author"
        return self.config.get("description", _description)

class Package():
    @staticmethod
    def add(package, reponame, action, arches, dist=None, sha=None, version=None, force=False):
        def _callback(result, dist, arches, config):
            version = result.get('version', None)
            if version is None:
                return {'status': 'failed', 'message': result.get('message', 'dummy')}
            if ShuttleJob.Package.selectBy(pkgname=pkgname, reponame=reponame, pkgver=version, action=action).count() != 0:
                os.system("rm -rf %s" % result['path'])
                pkg = ShuttleJob.Package.selectBy(pkgname=pkgname, reponame=reponame, pkgver=version, action=action).orderBy("-id")[0]
                #if job has builded, then skip rebuild
                if pkg.upload_status != UploadStatus.UNKNOWN:
                    return {'id': pkg.id, 'status': 'failed', 'messsage': 'same version has alread built - status: %d' % pkg.upload_status} 
                else:
                    for job in pkg.jobs:
                        job.status = JobStatus.WAIT
                    return {'status': 'successful', 'messsage': 'rebuild version has alread built'} 

            hashsum = result.get('hashsum', None)
            with lock:
                package = ShuttleJob.Package(pkgname=pkgname, reponame=reponame, pkgver=version, action=action, hashsum=hashsum)
            try:
                source_cache = os.path.join(cfg.get('build', 'cachedir'), 'debian-package', str(package.id), 'source')
                if not os.path.exists(source_cache):
                    os.makedirs(source_cache)
                for _file in result['files']:
                    os.system("install -Dm644 %s %s" % (os.path.join(result['path'], _file), os.path.join(source_cache, _file)))
                    # parse dsc support arches
                    if _file.endswith(".dsc"):
                        _arches = ['all']
                        with open(os.path.join(source_cache, _file)) as fp:
                            for line in fp:
                                if line.startswith('Architecture: '):
                                    _arches = line.split(':')[1].strip().split(',')
                                    break

                info = {'build': datetime.now().strftime("%Y-%m-%d %H:%M"),
                        'pkgname': pkgname,
                        'dist': dist,
                        'arches': arches,
                        'reponame': reponame,
                        'pkgver': version
                        }

                division = None
                try:
                    divisions = config.get('divisions', None)
                    if divisions is not None:
                        division = divisions.get(dist, None)
                except Exception as e:
                    pass

                if division is not None:
                    info['division'] = division

                if config.get('skip_source', None) == '1':
                    info['skip_source'] = '1'

                with open(os.path.join(source_cache, "source.info"), "w") as fp:
                    fp.write(json.dumps(info, indent=4))
                    
            finally: 
                os.system("rm -rf %s" % result['path'])

            if _arches[0] == "all":
                ShuttleJob.Job(package=package, arch=arches[0], dist=dist, status=JobStatus.WAIT)
            else:
                for arch in arches:
                    ShuttleJob.Job(package=package, arch=arch, dist=dist, status=JobStatus.WAIT)

            ShuttleLog.info("Add %s %s to %s" % (pkgname, version, reponame))
            if action == "tag":
                rebuild_packages = config.get("rebuild", [])
                for rebuild_name in rebuild_packages:
                    if ShuttleJob.Package.selectBy(pkgname=rebuild_name, reponame=reponame, action="tag").count() == 0:
                        continue
                    with lock:
                        rebuild_package = ShuttleJob.Package.selectBy(pkgname=rebuild_name, reponame=reponame, action="tag").orderBy("-id")[0]
                    if ShuttleJob.Package.selectBy(pkgname=rebuild_name, pkgver=rebuild_package.pkgver, reponame=reponame, action="rebuild").count() == 0:
                        binnmu = 1
                    else:
                        last_rebuild = ShuttleJob.Package.selectBy(pkgname=rebuild_name, pkgver=rebuild_package.pkgver, reponame=reponame, action="rebuild").orderBy("-id")[0]
                        binnmu = int(last_rebuild.binnmu) + 1

                    with lock:
                        save_rebuild = ShuttleJob.Package(pkgname=rebuild_name, reponame=reponame, pkgver=rebuild_package.pkgver, hashsum=rebuild_package.hashsum, action="rebuild", binnmu=binnmu)

                    _cache_dir = cfg.get('build', 'cachedir')
                    save_rebuild_cache = os.path.join('debian-package', str(save_rebuild.id)) 
                    rebuild_source_cache = os.path.join('debian-package', str(rebuild_package.id), 'source')
                    if os.path.exists(os.path.join(_cache_dir, save_rebuild_cache)):
                        os.system("rm -rf %s" % os.path.join(_cache_dir, save_rebuild_cache))
                    os.makedirs(os.path.join(_cache_dir, save_rebuild_cache))
                    os.system("cd %s && ln -rsf %s %s" % (_cache_dir, rebuild_source_cache, save_rebuild_cache))

                    save_rebuild.add_dep(package)
                    for arch in arches:
                        ShuttleJob.Job(package=save_rebuild, arch=arch, dist=dist, status=JobStatus.WAIT)

            return {'id': package.id, 'status': 'successful', 'version': version, 'pkgname': pkgname, 'reponame': reponame}

        # start main function
        try:
            if isinstance(package, dict):
                pkgname = package.get('pkgname')
                config = package
            else:
                pkgname = package
                config = PackageConfig(pkgname, reponame).config
        except Exception as e:
            return {'status': 'failed', 'message': 'package config can not loaded: %s' % json_config}

        builder = GitBuilder(pkgname=pkgname, config=config)
        builder.initial()

        if reponame is None:
            reponame = config.get('ppa', None)

        if reponame is None:
            return  {'status': 'failed', 'message': 'reponame should specify'}

        if action in ['commit', 'nightly']:
            if dist is None:
                dist = 'experimental'
            result = builder.archive(action, sha, "low", force)
        elif action == 'tag':
            if dist is None:
                dist = 'unstable'
            if version is None: 
                version = builder.get_release_version(url=builder.source_url, ref=sha, cwd=builder.source_cache)['ver']
            result = builder.tag(version, None, reponame)
        elif action == 'rc':
            if dist is None:
                dist = 'rc-buggy'
            if sha is None: 
                return {'status': 'failed', 'message': 'rc release must with a speciy sha and version'}
            result = builder.archive('rc', sha) 

        return _callback(result, dist, arches, config)

    @staticmethod
    def delete(pkgid):
        pkgs = shuttle.get_all_packages(id=pkg)
        if len(pkgs) != 0:
            pkg = pkgs[0]
            jobs = shuttle.get_all_jobs(packageID=pkg.id)
            for job in jobs:
                job.destroySelf()
            pkg.destroySelf()
            ShuttleLog.info("Delete %s %s" % (pkg.pkgname, pkg.pkgver))
            return {'status': 'successful'}
        else:
            return {'status': 'failed'}

    @staticmethod
    def requeue_upload(pkgid):
        if ShuttleJob.Package.selectBy(id=pkgid).count() == 0:
            return {'status': 'skip'}

        pkg = ShuttleJob.Package.selectBy(id=pkgid)[0]
        if pkg.upload_status > UploadStatus.UPLOADING:
            ShuttleLog.info("Requeue %s %s from %s to WAIT" % (pkg.pkgname, pkg.pkgver, UploadStatus.whatis(pkg.upload_status)))
            pkg.upload_status = UploadStatus.WAIT
        else:
            ShuttleLog.info("Skip requeue %s %s with %s"  % (pkg.pkgname, pkg.pkgver, UploadStatus.whatis(pkg.upload_status)))
        return {'status': 'successful'}

class LoopAddPackage():
    def __init__(self, reponame):
        self.packages = []
        self.configdir=cfg.get('build', 'configdir')
        self.reponame = reponame
        self.package_config = os.path.join(self.configdir, 'package', reponame)
        if not os.path.exists(self.package_config):
            print("W: %s is not exists.Set as defaults" % self.package_config)
            self.package_config = os.path.join(self.configdir, 'package', 'defaults')

        for _json in glob.glob(os.path.join(self.package_config, '*.json')):
            package = os.path.basename(_json)[:-5]
            if package not in ['default']:
                self.packages.append(package)

    def get_version(self, pkgname, ref=None):
        if pkgname not in self.packages:
            return None
        pkgconfig = os.path.join(self.package_config, '%s.json' % pkgname)
        config = json.load(open(pkgconfig, "r"))
        package = GitBuilder(pkgname, config)
        package.initial()
        if ref is None:
            version = package.get_release_version(url=package.source, ref=package.source_ref, cwd=package.source_cache)
        else:
            version = package.get_release_version(url=package.source, ref=ref, cwd=package.source_cache)

        return version

    def loop(self, ignore_commit=True, arches=['amd64', 'i386']):
        for package in self.packages:
            try:
                pkgver =  self.get_version(package)
            except Exception as e:
                print("%s Error: %s" %  (package, e))
                pkgver = None
            if pkgver is None:
                continue
            if pkgver['fallback'] is False:
                hashsum = self.get_version(package, pkgver['ver'])['sha']
                if ShuttleJob.Package.selectBy(pkgname=package, reponame=self.reponame, action='tag', hashsum=hashsum).count() == 0:
                    Package().add(package=package, reponame=self.reponame, sha=pkgver['sha'], action="tag", arches=arches)

            if not ignore_commit:
                comver = "%(ver)s+r%(rev)s~%(sha)s" % pkgver
                if ShuttleJob.Package.selectBy(pkgname=package, reponame=self.reponame, pkgver=comver).count() == 0:
                    with lock:
                        Package().add(package=package, reponame=self.reponame, sha=pkgver.get('sha', None), action="commit")
