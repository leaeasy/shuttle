#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

import logging
import os
import re

from ShuttleConfig import ShuttleConfig

class ShuttleLog(object):

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init()
        return cls._instance

    def init(self):
        cfg = ShuttleConfig()
        logging.basicConfig(level = logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            filename=cfg.get('log', 'file'),
                            datefmt=cfg.get('log', 'time_format'),
                            filemode='a')
    @classmethod
    def info(self, str):
        logging.info(str)

    @classmethod
    def warn(self,str):
        logging.warning(str)

    @classmethod
    def error(self, str):
        logging.error(str)

APT_MISSING_DEP_PATTERNS = [
    'but [^ ]* is to be installed',
    'but [^ ]* is installed',
    'but it is not installable',
    'which is a virtual package .*',
    ]

class DebianBuildLogRegexes:
    """Build log regexes for performing actions based on regexes, and extracting dependencies for auto dep-waits"""

    GIVENBACK = [
	("^E: There are problems and -y was used without --force-yes"),
        ("^E: Unable to fetch some packages"),
	]
    DEPFAIL = {
	'The following packages have unmet dependencies:\n'
	'.* Depends: (?P<p>[^ ]*) (%s)\n'
	% '|'.join(APT_MISSING_DEP_PATTERNS): "\g<p>",
	}
    BUILDFAIL = [
	("^dpkg-buildpackage: error: debian/rules build gave error exit status")
	]

class DebianLogParser(object):
    def __init__(self, basepath):
	self.basepath = basepath

    def searchLogContents(self, logfile, patterns_and_flags, stop_patterns_and_flags=[]):
        chunk_size = 256 * 1024
        regexes = [
                re.compile(pattern, flags) for pattern, flags in patterns_and_flags
                ]
        stop_regexes = [
                re.compile(pattern, flags) for pattern, flags in stop_patterns_and_flags
                ]

        fp = open(logfile)
        try:
            window = ""
            chunk = fp.read(chunk_size)
            while chunk:
                window += chunk
                for regex in regexes:
                    match = regex.search(window)
                    if match is not None:
                        return regex.pattern, match
                for regex in stop_regexes:
                    if regex.search(window) is not None:
                        return None, None
                if len(window) > chunk_size:
                    window = window[chunk_size:]
                chunk = fp.read(chunk_size)
        finally:
            fp.close()

        return None, None


    def parser_buildlog(self, logfile="buildlog"): 
        log_patterns = []
        stop_patterns = [["^I: unmounting dev/pts filesystem", re.M]]

        buildlog = os.path.join(self.basepath, logfile)

        if not os.path.exists(buildlog):
            return "Unkonwn build status: No buildlog found!"

        for rx in DebianBuildLogRegexes.GIVENBACK:
            log_patterns.append([rx, re.M])
        for rx in DebianBuildLogRegexes.DEPFAIL:
            log_patterns.append([rx, re.M | re.S])
        for rx in DebianBuildLogRegexes.BUILDFAIL:
            log_patterns.append([rx, re.M | re.S])

        missing_dep = None

        if log_patterns:
            rx, mo = self.searchLogContents(buildlog, log_patterns, stop_patterns)
            if mo is None:
                # It was givenback, but we can't see a valid reason.
                # Assume it failed.
                success = "GIVENBACK"
            elif rx in DebianBuildLogRegexes.DEPFAIL:
                # A depwait match forces depwait.
                missing_dep = mo.expand(DebianBuildLogRegexes.DEPFAIL[rx])
                success = "DEPFAIL"
            elif rx in DebianBuildLogRegexes.BUILDFAIL:
                success = "BUILDFAIL"
            else:
                # Otherwise it was a givenback pattern, so leave it
                # in givenback.
                success = "GIVENBACK"

        if missing_dep is not None:
            msg = "Returning build status: DEPFAIL\n"
            msg += "Dependencies: %s " % missing_dep
        elif success == "GIVENBACK":
            msg = "Returning build status: GIVENBACK"
        elif success == "BUILDFAIL":
            msg = "Returning build status: PACKAGEFAIL"
        else:
            msg = "Unknown build status."
        return msg
