from twisted.internet import reactor
from ShuttleSlave import ShuttlePbuildImageSlave
slave = ShuttlePbuildImageSlave('pbuilder', 'http://127.0.0.1:8221')
def cleanSlave(ignore):
    slave.clean()

if slave.status().get("builder_status") == "BuilderStatus.IDLE":
    slave.build('1234', 'pbuilder', 'unstable', 'i386', 'http://127.0.0.1/shuttle/config/builder', 'default.json')
if slave.status().get("build_status") == "BuildStatus.OK":
    files = slave.getFiles(["buildlog"],"/tmp/buildlog", '1234')
    #files.addCallback(cleanSlave)
    print(slave.status())

reactor.run()
