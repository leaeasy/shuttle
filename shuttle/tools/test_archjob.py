#!/usr/bin/env python
import sqlobject
from sqlobject.sqlite import builder
from ShuttleJob import JobStatus, PbuildStatus
conn = builder()('/tmp/shuttle.db')
sqlobject.sqlhub.processConnection = conn

import ShuttleJob

ShuttleJob.Package.createTable(ifNotExists=True)
ShuttleJob.Job.createTable(ifNotExists=True)
ShuttleJob.Pbuild.createTable(ifNotExists=True)

#package = ShuttleJob.Package(pkgname="lightdm", pkgver="1.19.4-5")
#ShuttleJob.Job(arch="amd64", package=package, status=JobStatus.WAIT)
#ShuttleJob.Job(arch="i386", package=package, status=JobStatus.WAIT)

#package = ShuttleJob.Package(pkgname="startdde", pkgver="1.0~beta-1", reponame="dde")
#ShuttleJob.Job(arch="amd64", package=package, status=JobStatus.WAIT)

ShuttleJob.Pbuild(jobname="default", dist="unstable", arch="amd64", status=PbuildStatus.WAIT)


#for job in DebianJob.Job.selectBy():
#    print(job.package.id)
#
#for package in ArchJob.Package.selectBy():
#    print(package.id)
#    print(package.jobs.count())
#
#    for job in package.jobs:
#        print(job.package.id)
