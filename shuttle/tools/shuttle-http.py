#!/usr/bin/env python

from Shuttle import Shuttle, Builder
from ShuttleConfig import ShuttleConfig
from threading import Thread
from datetime import date
import ShuttleJob 

from datetime import datetime

from twisted.internet.task import LoopingCall

from twisted.internet import reactor

shuttle = Shuttle()
cfg = ShuttleConfig()

loop = LoopingCall(shuttle._loop)
loop.start(cfg.getint('build', 'check_every'))

reactor.run()
