import glob
import os
import subprocess
import datetime
import sys

import sqlobject
from sqlobject.sqlite import builder
from ShuttleJob import JobStatus

conn = builder()('/srv/shuttle/shuttle.db')
sqlobject.sqlhub.processConnection = conn

import ShuttleJob
ShuttleJob.Package.createTable(ifNotExists=True)
ShuttleJob.Job.createTable(ifNotExists=True)


SOURCE_DIR = '/srv/shuttle/source'

class DscFile(object):
    def __init__(self, dscfile):
        self.archs = []
        self.files = []
        self.dscfile = dscfile
        self.basepath = os.path.dirname(os.path.abspath(self.dscfile))
        self.init()

    def init(self):
        fileflag = False
        with open(self.dscfile) as fp:
            for line in fp.readlines():
                if line.endswith('\n'):
                    line = line[:-1]
                if line.startswith('Source: '):
                    self.source = line.split(':')[1].strip()
                    continue
                if line.startswith('Version: '):
                    line = line.replace("Version :", "")
                    self.version = line.split(':')[-1].strip()
                    continue
                if line.startswith('Architecture: '):
                    self.archs = line.split(':')[1].strip().split(',')
                    continue
                if not fileflag and line.startswith('Files:'):
                    fileflag = True
                elif fileflag: 
                    if not line.startswith(' '):
                        fileflag =  False
                    else:
                        filename = line.split(' ')[-1]
                        self.files.append(filename)
        self.files.append(os.path.basename(self.dscfile))

    def echo(self):
        print(self.source, self.version, self.archs, self.files, self.basepath)


if __name__ == "__main__":
    dsc = DscFile(sys.argv[1])
    dsc.echo()
    archs = ['amd64']
    #for arch in dsc.archs:
    #    if arch == 'any':
    #        archs = ['amd64', 'i386']
    #    elif arch == 'all':
    #        if 'amd64' not in archs:
    #            archs.append('amd64')
    #    else:
    #        if arch not in archs:
    #            archs.add(arch)
    dest_dir = os.path.join(SOURCE_DIR, dsc.source)
    for file in dsc.files:
        print("Add file to source: %s" % file)
        os.system("install -Dm644 %s %s" %(os.path.join(dsc.basepath, file), os.path.join(dest_dir, file)))
    package = ShuttleJob.Package(pkgname=dsc.source, pkgver=dsc.version, reponame='debian')
    for arch in archs:
        ShuttleJob.Job(arch=arch, package=package, status=JobStatus.WAIT)

