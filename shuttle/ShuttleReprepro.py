import glob
import os

from ShuttleConfig import ShuttleConfig
import json
from twisted.internet import reactor, threads, defer
from twisted.internet.task import LoopingCall
import sqlobject
import subprocess
import ShuttleJob
from ShuttleJob import UploadStatus
from ShuttleLog import ShuttleLog
import threading

from ShuttleNotify import ShuttleNotify

class ShuttleRepo(object):
    tasks = []
    task  = None
    max_pending = 5
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init()
        return  cls._instance

    def init(self):
        ShuttleLog()
        for package in ShuttleJob.Package.selectBy(upload_status=UploadStatus.UPLOADING).orderBy('-id'):
            package.upload_status = UploadStatus.WAIT
    
    def push(self, task):
        deferred = defer.Deferred()
        self.tasks.append(task)
        self.process()
        return deferred

    def finished(self, *args):
        ShuttleLog.info("Upload %s %s to %s OK" % (self.task.pkgname, self.task.pkgver, self.task.reponame))
        self.task.upload_status = UploadStatus.UPLOAD_OK
        self.task = None
        self.process()

    def failed(self, *args):
        message = "Upload %s %s to %s Failed" % (self.task.pkgname, self.task.pkgver, self.task.reponame)
        ShuttleLog.warn(message)
        self.task.upload_status = UploadStatus.UPLOAD_FAILED
        self.task = None

        if ShuttleNotify().notify_method.get("bearychat", None) is not None:
            ShuttleNotify().notify("bearychat", message)

        self.process()
    
    def process(self):
        if self.task or len(self.tasks) == 0:
            return
        self.task = self.tasks.pop(0)
        d = self.include_packages(self.task)
        d.addCallback(self.finished)
        d.addErrback(self.failed)

    def include_packages(self, task):
        reponame = str(task.reponame)

        cache_dir = os.path.join(ShuttleConfig().get('build', 'cachedir'), 'debian-package', str(task.id))
        source_info = os.path.join(cache_dir, 'source', 'source.info')
        config = None
        try:
            config = json.load(open(source_info))
        except Exception as e:
            config = {}

        division = config.get('division', None)
        repo = Reprepro(reponame, division)

        t = threads.deferToThread(repo.include_packages, cache_dir, config)
        return t

    def run(self):
        for package in ShuttleJob.Package.selectBy(upload_status=UploadStatus.WAIT).orderBy('-id'):
            if len(self.tasks) < self.max_pending:
                self.package = UploadStatus.UPLOADING
                self.push(package)

class Reprepro():
    def __init__(self, name, division=None):
        json_config = os.path.join(ShuttleConfig().get('repos', 'config'),  "%s.json" % name)
        if division is None:
            self.repo_path = os.path.join(ShuttleConfig().get('repos', 'basedir'), name)
        else:
            self.repo_path = os.path.join(ShuttleConfig().get('repos', 'basedir'), 'ppa', name, division)

        self.config = json.load(open(json_config, "r"))
        self.name = self.config.get("name")
        self.dists = self.config.get("dists")
        self.arches = self.config.get("arches")

        self.post_process = self.config.get("post_process", None)

    def config_exists(self):
        reprepro_path_conf_file = os.path.join(self.repo_path, "db", "packages.db")
        return os.path.exists(reprepro_path_conf_file)

    def create_config(self):
        reprepro_path_conf = os.path.join(self.repo_path, "conf")
        if not os.path.exists(reprepro_path_conf):
            os.makedirs(reprepro_path_conf)

        config_file = open(os.path.join(reprepro_path_conf, "distributions"), "w")
        for dist in self.dists:
            config_file.write("Codename: %s\n" % dist)
            config_file.write("Components: main\n")
            config_file.write("UDebComponents: main\n")

            arches = self.config.get("%s-arches" % dist, None)
            if arches is None:
                arches = self.arches

            if self.config.get("gpg_sign") == "1":
                config_file.write("SignWith: yes\n")
            if self.config.get("skip_source") == "1":
                config_file.write("Architectures: %s\n" % " ".join(arches))
            else:
                config_file.write("Architectures: %s source\n" % " ".join(arches))
                config_file.write("DscIndices: Sources Release . .gz .bz2\n")
            config_file.write("DebIndices: Packages Release . .gz .bz2\n")
            config_file.write("\n")

        config_file.close()

        os.system("reprepro --basedir %s export" % self.repo_path)

    def runSubprocess(self, cmd, log):
        try:
            proc = subprocess.Popen(cmd, shell=True, bufsize=0, stdout=subprocess.PIPE, stdin=None, stderr=subprocess.STDOUT)
        except Exception as err:
            log.write("\nUnable to execute command \"%s\": %s\n" % (cmd, err))
            raise

        stdout, _ = proc.communicate()

        for line in stdout:
            log.write(line)

        if proc.returncode != 0:
            log.write("Failed execute \"%s\" at %s\n" % (cmd, sqlobject.DateTimeCol.now()))

    def include_packages(self, cache_dir, config):
        dists = []
        if not os.path.exists(cache_dir):
            return
        for file in os.listdir(cache_dir):
            for dist in self.dists:
                if file.startswith(dist):
                    if dist not in dists:
                        dists.append(dist)

        logfile = os.path.join(cache_dir, "log")
        if os.path.exists(logfile):
            os.remove(logfile)

        with open(logfile, "a") as log:
            if not self.config_exists():
                log.write("Creating repo first\n")
                self.create_config()
            log.write("Start at %s\n\n" % sqlobject.DateTimeCol.now())

            for dist in dists:
                log.write(" ... starting upload %s\n" % dist)

                if dist not in self.dists:
                    log.write(" Ignore dist: %s is not supported\n" % dist)
                    continue

                arches = self.config.get("%s-arches" % dist, None)
                if arches is None:
                    arches = self.arches

                if self.config.get("lintian") == "1":
                    log.write("Run lintian to checkif result is valid... \n")
                    for arch in arches: 
                        changes = os.path.join(cache_dir, "%s-%s" % (dist, arch), "*.changes")
                        if len(glob.glob(changes)) > 0:
                            log.write(" Checking %s %s\n" % (dist, arch))
                            cmd = "lintian -I --show-overrides %s" % changes
                            self.runSubprocess(cmd, log)

                if self.config.get("skip_source") != "1" and config.get("skip_source", "0") != "1":
                    source_dir = os.path.join(cache_dir, "source")
                    dsc_files = glob.glob(os.path.join(source_dir, "*.dsc"))
                    for dsc_file in dsc_files:
                        log.write(" Check source if already include by past\n")
                        source = os.path.basename(dsc_file).split('_')[0]
                        cmd = "reprepro -T dsc -C main --basedir %(basedir)s listfilter %(dist)s 'Package (==%(source)s)'" % \
                        {
                            "basedir": self.repo_path,
                            "dist": dist,
                            "source": source
                        }
                        with open(os.devnull, 'w') as FNULL:
                            proc = subprocess.Popen(cmd, shell=True, bufsize=0, stdout=subprocess.PIPE, stdin=None, stderr=FNULL)
                        stdout, _ = proc.communicate()
                        if proc.returncode == 0:
                            stdout = stdout[:-1].split('\n')[0]
                            if stdout.startswith('%s|' % dist):
                                included_version = stdout.split(' ')[2].split('-')[0]
                                uploaded_version = os.path.basename(dsc_file).split('_')[1].split('-')[0]
                                if included_version == uploaded_version:
                                    log.write(" remove duplicate version %s %s\n" % (source, included_version))
                                    cmd = "reprepro -V --basedir %(basedir)s removesrc %(dist)s %(source)s" % \
                                    {
                                        "basedir": self.repo_path,
                                        "dist": dist,
                                        "source": source
                                    }
                                    self.runSubprocess(cmd, log)

                    cmd = "reprepro -V --basedir %(basedir)s -C main includedsc %(dist)s %(dsc)s" % \
                    {
                        "basedir": self.repo_path,
                        "dist": dist,
                        "dsc": os.path.join(source_dir, "*.dsc")
                    }
                    try:
                        self.runSubprocess(cmd, log)
                    except Exception as e:
                        pass
                
                for arch in arches:
                    if os.path.exists(os.path.join(cache_dir, "%s-%s" % (dist, arch))):
                        all_debs = os.path.join(cache_dir, "%s-%s" % (dist, arch), "*all.deb")
                        if len(glob.glob(all_debs)) > 0:
                            cmd = "reprepro -V --basedir %(basedir)s -C main includedeb %(dist)s %(deb)s" % \
                                {
                                    "basedir": self.repo_path,
                                    "dist": dist,
                                    "deb": all_debs
                                }
                            self.runSubprocess(cmd, log)
                        break
                
                # add other arch-specific packages
                for arch in arches:
                    arch_debs = os.path.join(cache_dir, "%s-%s" % (dist, arch), "*" + arch + ".deb")
                    if len(glob.glob(arch_debs)) > 0:
                        cmd =  "reprepro -V --basedir %(basedir)s -C main includedeb %(dist)s %(deb)s" % \
                        {
                            "basedir": self.repo_path,
                            "dist": dist,
                            "deb": arch_debs
                        }
                        self.runSubprocess(cmd, log)

                if cmd is None:
                    log.write("Nothing to upload\n")

                log.write("Finished at %s\n" % sqlobject.DateTimeCol.now())

            if self.post_process is not None:
                log.write("Run post process ... \n")
                for cmd in self.post_process:
                    try:
                        self.runSubprocess(cmd, log)
                    except Exception as e:
                        log.write("%s" % e)
                        pass

if __name__ == "__main__":
    #from sqlobject.sqlite import builder
    #conn = builder()('/tmp/shuttle.db')
    #sqlobject.sqlhub.processConnection = conn
    #repo = LoopingCall(ShuttleRepo().run)
    #repo.start(10)
    #reactor.run()
    import sys
    reponame = sys.argv[1]
    taskid = sys.argv[2]
    cache_dir = os.path.join(ShuttleConfig().get('build', 'cachedir'), 'debian-package', str(taskid))
    source_info = os.path.join(cache_dir, 'source', 'source.info')
    config = None
    try:
        config = json.load(open(source_info))
    except Exception as e:
        config = {}
    division = config.get('division', None)
    repo = Reprepro(reponame, division)
    repo.include_packages(cache_dir, config)
    #r.create_config()
