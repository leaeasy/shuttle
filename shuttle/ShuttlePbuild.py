#!/usr/bin/env python

import threading
from ShuttleLog import ShuttleLog
import sqlobject
from ShuttleJob import PbuildStatus
from ShuttleConfig import ShuttleConfig
from ShuttleSlave import ShuttlePbuildImageSlave 
import ShuttleJob
import signal
import sys
import glob
from datetime import datetime
import socket
import os

class ShuttlePbuilder(object):
    slaves = []
    jobs = []
    _instance = None
    builder = 'pbuilder'

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init()
        return cls._instance

    def init(self):
        self.cfg = ShuttleConfig()
        ShuttleLog()
        self._sqlconnection = sqlobject.connectionForURI(self.cfg.get('build', 'database_uri'))
        sqlobject.sqlhub.processConnection = self._sqlconnection

        self.jobs_locker = threading.Lock()
        self.do_quit = threading.Event()

    def loop(self):
        self.flog_slaves()
        self.start_jobs()
        self.clean_jobs()
        self.do_quit.wait(1)

    def _loop(self):
        counter = self.cfg.getint('build', 'check_every')
        while not self.do_quit.isSet():
            if counter == self.cfg.getint('build', 'check_every'):
                self.flog_slaves()
                self.start_jobs()
                self.clean_jobs()
                counter = 0
            self.do_quit.wait(1)
            counter += 1

    def daemon(self):
        ShuttleLog.info("Starting Shuttle Pbuilder")
        self.daemonize()

        ShuttleLog.info("Run main loop")
        self.loop()

        ShuttleLog.info("Release wait locked jobs")
        self.release_jobs()

        ShuttleLog.info("Exiting rebuildd")

    def daemonize(self):
        signal.signal(signal.SIGTERM, self.handle_sigterm)
        signal.signal(signal.SIGINT, self.handle_sigterm)
        try:
            sys.stdout = sys.stderr = open(self.cfg.get('log', 'file'), 'a')
        except Exception as error:
            print("E: unable to open logfile: %s" % error)


    def release_jobs(self):
        with self.jobs_locker:
            for job in self.jobs:
                with job.status_lock:
                    if job.status == PbuildStatus.WAIT_LOCKED:
                        job.status = PbuildStatus.WAIT
        return True


    def add_slave(self, slave):
        for _slave in self.slaves:
            if _slave.name == slave.name:
                ShuttleLog.warn("Slave %s is already exists" % slave.name)
                return

        self.slaves.append(slave)

    def add_job(self, field, name, dist, arch=None):
        if not arch:
            arch = [self.cfg.arch]

        ShuttleJob.Pbuild(field=field, jobname=name, dist=dist, arch=arch, status=PbuildStatus.WAIT)
        return 

    def get_all_jobs(self, **kwargs):
        jobs = []
        jobs.extend(ShuttleJob.Pbuild.selectBy(**kwargs))
        return jobs

    def flog_slaves(self):
        for slave in self.slaves:
            status = slave.status()
            if status.get('builder_status', None) == 'BuilderStatus.OFFLINE':
                continue

            if status.get('builder_status', None) == 'BuilderStatus.WAITING' and slave.uploading is False:
                buildid = status.get('build_id', None)
                if buildid is None:
                    continue
                job = self.get_all_jobs(id=buildid)[0]
                if status.get('build_status') == "BuildStatus.OK":
                    ShuttleLog.info("Set %s %s %s:%s build status: %s" % (job.jobname, job.build, job.dist, job.arch, PbuildStatus.whatis(PbuildStatus.BUILD_OK)))
                    job.status = PbuildStatus.BUILD_OK
                else:
                    ShuttleLog.info("Set %s %s %s:%s build status: %s" % (job.jobname, job.build, job.dist, job.arch, PbuildStatus.whatis(PbuildStatus.FAILED)))
                    job.status = PbuildStatus.FAILED
                slave.complete()
            slave.refresh()

    def start_jobs(self):
        with self.jobs_locker:
            for slave in self.slaves:
                status = slave.status()
                if status.get('builder_status', None) != 'BuilderStatus.IDLE':
                    continue

                for job in ShuttleJob.Pbuild.selectBy(status=PbuildStatus.WAIT):
                    if job.dist in slave.dists and job.arch in slave.arches:
                        job.status = PbuildStatus.WAIT_LOCKED
                        ShuttleLog.info("Launch %s to build %s %s" % (slave.name, job.jobname, job.arch))
                        job.start(slave, self.builder)
                        self.jobs.append(job)
                        break

    def clean_jobs(self):
        for job in self.jobs:
            if job.status >= PbuildStatus.CONFIG_FAILED:
                self.jobs.remove(job)

    def handle_sigterm(self, signum, stack):
        ShuttleLog.info("Receiving transmission... it's a signal %s capt'ain! EVERYONE OUT!" % signum) 
        self.do_quit.set()

if __name__ ==  "__main__":
    shuttle = ShuttlePbuilder()
    #shuttle.add_job(name='default', dist='unstable', arch='i386')
    #shuttle.add_job(name='default', dist='unstable', arch='amd64')
    shuttle.add_job(name='go', dist='unstable', arch='amd64')
    shuttle.add_job(name='qt5', dist='unstable', arch='amd64')
    shuttle.cfg.set("build",  "check_every",  "3")
    slave = ShuttlePbuildImageSlave('pbuilder', 'http://127.0.0.1:8221/')
    shuttle.add_slave(slave)
    shuttle.loop()
    from twisted.internet import  reactor
    from twisted.internet.task import LoopingCall
    loop = LoopingCall(shuttle.loop)
    loop.start(10)
    reactor.run()
