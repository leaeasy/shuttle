#!/usr/bin/env python

from ShuttleConfig import ShuttleConfig
import os
import subprocess
import tempfile
import time
import ShuttleJob
from threading import Timer

def get_builder_name(default="shuttle"):
    return os.environ.get("DEBFULLNAME", default)

def get_builder_email(default="shuttle@deepin.com"):
    return os.environ.get("DEBEMAIL", default)

def get_changelog_time():
    return time.strftime("%a, %d %b %Y %T %z", time.localtime())

class Dch():
    def __init__(self, debian_repo):
        self.debian_repo = debian_repo

    def mangle_changelog(self, cp, revision, content):
        try:
            with open(os.path.join(self.debian_repo, 'debian', 'changelog.dch'), 'w') as cw:
                cw.write("%(Source)s (%(Version)s) %(Distribution)s; urgency=%(Urgency)s\n" % cp)
                cw.write("\n")
                cw.write("  ** Build revesion: %s **\n\n" % revision)

                if isinstance(content, str):
                    cw.write("  * %s\n" % content)
                    cw.write("\n")
                if isinstance(content, list):
                    cw.write("  * detail lastest %d commit logs below: \n" % (len(content)))
                    for i in  content:
                        cw.write("   - %s\n" % i)
                    cw.write("\n")
                if isinstance(content, dict):
                    for key in content:
                        cw.write("  [%s]\n" % key)
                        for value in content.get(key):
                            cw.write("  * %s\n" % value)
                        cw.write("\n")
                    cw.write("\n")
                cw.write(" -- %s <%s>  %s\n" % (get_builder_name(), get_builder_email(), get_changelog_time()))
            os.rename(os.path.join(self.debian_repo, 'debian', 'changelog.dch'), os.path.join(self.debian_repo,  'debian', 'changelog'))
        except Exception as err:
            print(err)
            return False
        return True


class GitBuilder():
    def __init__(self, pkgname, config):
        '''Config is a dict, like as
        {
            "source": "https://cr.deepin.io/dde/dde-daemon#branch=master",
            "debian": "https://cr.deepin.io/dde/dde-daemon#branch=debian",
            "builder": "default"
        }

        '''
        self.pkgname = pkgname
        self.config  = config
        self._cache = ShuttleConfig().get('source', 'cache_dir')

    def execute(self, subprocess_args, cwd=None, extended_output=False, timeout=30):
        if cwd is None:
            cwd = self.source_cache

        env = os.environ.copy()
        env["LC_MESSAGE"] = "C"

        if isinstance(subprocess_args, str):
            subprocess_args = subprocess_args.split()
        proc = subprocess.Popen(subprocess_args, env=env, cwd=cwd, stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=None)
        kill_proc = lambda p: p.kill()
        timer = Timer(timeout, kill_proc, [proc])
        timer.start()
        while proc.poll() is None:
            time.sleep(0.1)
        timer.cancel()

        status = proc.returncode
        if status != 0:
            raise Exception("Subprocess  \"%s\" Error: %d at %s\n"  % (" ".join(list(subprocess_args)), status, cwd))
        
        stdout = proc.stdout.read()
        stderr = proc.stderr.read()
        if stdout.endswith('\n'):
            stdout = stdout[:-1]
        if stderr.endswith('\n'):
            stderr = stderr[:-1]

        if extended_output:
            return (status, stdout, stderr)
        else:
            return stdout

    def update_cache(self):
        if os.path.exists(self.source_cache):
            self.execute(['git', 'fetch', 'origin', '+refs/heads/*:refs/heads/*', '--prune', '--tags'], cwd=self.source_cache)
        else:
            self.execute(['git', 'clone', '--bare', self.source_url, self.pkgname ], cwd=self._cache)

        if os.path.exists(self.debian_cache):
            self.execute(['git', 'fetch', 'origin', '+refs/heads/*:refs/heads/*', '--prune'], cwd=self.debian_cache)
        else:
            self.execute(['git', 'clone', '--bare', self.debian_url, self.pkgname ], cwd=os.path.join(self._cache,'debian'))

    @staticmethod
    def parser_url(url):
        if '#branch=' in url:
            url, ref = url.split("#branch=")
        elif '#commit=' in url: 
            url, ref = url.split("#commit=")
        elif '#tag=' in url:
            url, ref = url.split("#tag=")
        else:
            url = url
            ref = 'HEAD'
        return (url, ref)

    def initial(self):
        try:
            self.source = self.config['source']
            self.debian = self.config['debian']
        except Exception as e:
            return 
            #raise OSError("config error: %s not exists" % e)

        self.source_url, self.source_ref = self.parser_url(self.source)
        self.debian_url, self.debian_ref = self.parser_url(self.debian)
        self.source_cache = os.path.join(self._cache, self.pkgname)
        if self.source_url == self.debian_url:
            self.debian_cache = self.source_cache
        else:
            self.debian_cache = os.path.join(self._cache, 'debian', self.pkgname)

        self.update_cache()

    def get_release_version(self, url, ref, cwd, fakeversion=False):
        if ref is None:
            ref = self.source_ref
        try:
            stdout = self.execute(['git', 'describe', '--tags', '--long', ref], cwd=cwd)
            [ver, rev, sha] = stdout.split("-")[:3]
            if ver.startswith('v'):
                ver = ver[1:]
            sha = sha[1:]
            fallback = False

        except Exception as e:
            print(e)
            fallback = True
            ver = "0.0"
            rev = self.execute(['git', 'rev-list', '--count', 'master'], cwd=cwd)
            sha = self.execute(['git', 'rev-parse', '--short', 'master'], cwd=cwd)

        #Maybe fakeversion is needed
        if fakeversion:
            lastest = self.execute(['git', 'rev-list', '--tags', '--max-count=1'], cwd=cwd)
            fakever = self.execute(['git', 'describe', '--tags', lastest], cwd=cwd)
            if fakever != ver:
                ver = "%s.is.%s" % (fakever, ver)

        return {"ver":ver, "rev":rev, "sha":sha[:7], "fallback": fallback}

    def log(self, sha, maxline):
        logs = self.execute(['git', 'log', "--pretty='%s'", sha, "-%d" % maxline])
        result = []
        for log in  logs.split('\n'):
            if log.startswith("'") and log.endswith("'"):
                log = log[1:-1]
            result.append(log)
        return result

    def _archive(self, temp_dir, release_action="release", ref=None, max_orig_changes=5, urgency="low", force=False): 
        '''
        release_action will in ['release-candidate', 'commit', 'release']
        '''
        if ref is None:
            ref = self.source_ref

        fakeversion = False
        if self.config.get("fakeversion", None):
            if self.config['fakeversion'].get('commit', '0') == '1':
                fakeversion = True

        pkgver = self.get_release_version(url=self.source_url, ref=ref, cwd=self.source_cache, fakeversion=fakeversion)
        _revision = pkgver['sha']

        if release_action in ['rc', 'release-candidate']:
            dist = 'rc-buggy'
            pkgver["ver"] = pkgver["ver"] + "+next"
        elif release_action in ['nightly-build', 'nightly']:
            dist = 'nightly'
            pkgver['sha'] += '.nightly'
        else:
            dist = 'experimental'

        _pkgver = "%(ver)s+r%(rev)s~%(sha)s" % pkgver
        if force:
            binnum = 0
            while True:
                version = _pkgver + '.b%d' % binnum if binnum else _pkgver
                try:
                    package = ShuttleJob.Package.selectBy(pkgname=self.pkgname, pkgver=version)
                except:
                    package = None
                if package and package.count():
                    binnum = binnum + 1
                else:
                    break
            _pkgver = version
            
        _prefix = "%s-%s/" % (self.pkgname, _pkgver)

        with open(os.devnull, 'w') as devnull:
            subprocess.check_call("git archive --format=tar --prefix=%s %s | (cd %s && tar xvf -)" % (_prefix, _revision, temp_dir), shell=True, cwd = self.source_cache, stdout=devnull, stderr=devnull)

        #TODO: fix debian cache
        if not os.path.exists(os.path.join(temp_dir, _prefix, 'debian')) or self.debian_url != self.source_url:
            debian = self.get_release_version(url=self.debian_url, ref=self.debian_ref, cwd=self.debian_cache)
            with open(os.devnull, 'w') as devnull:
                subprocess.check_call("git archive --format=tar --prefix=debian/ %s | (cd %s && tar xvf -)" % (debian['sha'], temp_dir), shell=True, cwd = self.debian_cache, stdout=devnull, stderr=devnull)

            _debian_dir = os.path.join(temp_dir, 'debian', 'debian')
            if not os.path.exists(_debian_dir):
                raise IOError("debian path is not exists") 
            subprocess.check_call("rm -rf %s" % (os.path.join(temp_dir, _prefix, 'debian')), shell=True)
            subprocess.check_call("cp -r %s %s" % (_debian_dir, os.path.join(temp_dir, _prefix)),shell=True)

        work_dir = os.path.join(temp_dir, _prefix)

        if not os.path.exists(os.path.join(work_dir, 'debian', 'source')):
            os.makedirs(os.path.join(work_dir, 'debian', 'source'))

        with open(os.path.join(work_dir, 'debian', 'source', 'format'), 'w') as fp:
            fp.write("3.0  (native)\n")

        content = self.log(_revision, maxline=max_orig_changes)

        dch =  Dch(os.path.join(work_dir))
        cp = {'Source': self.pkgname, 'Version': _pkgver, 'Distribution': dist, 'Urgency': urgency }
        dch.mangle_changelog(cp=cp, revision=_revision, content=content)

        subprocess.check_call('dpkg-source -b %s' % _prefix, shell=True, cwd=temp_dir)
        files = []
        for d in os.listdir(temp_dir):
            if os.path.isfile(os.path.join(temp_dir, d)):
                files.append(d)

        return _pkgver, temp_dir, files, _revision

    def archive(self, release_action="commit", ref=None, urgency="low", force=False, max_orig_changes=5):
        self.initial()
        temp_prefix = os.path.join("/tmp", "git-archive-temp")
        if not os.path.exists(temp_prefix):
            os.makedirs(temp_prefix)
        temp_dir =  tempfile.mkdtemp(dir=temp_prefix)
        try:
           _pkgver, _dir, _files, hashsum = self._archive(temp_dir, release_action=release_action, ref=ref, urgency=urgency, force=force,  max_orig_changes=max_orig_changes)
        except Exception as e:
            os.system("rm -rf %s" % temp_dir)
            return {'version': None, 'message': str(e)}

        return {'version': _pkgver, 'path': _dir, 'files': _files, 'hashsum': hashsum }

    def _tag(self, temp_dir, origin_ver, reponame, dist="unstable", urgency="low"):
        _revision = self.get_release_version(url=self.source_url, ref=origin_ver, cwd=self.source_cache)['sha']

        command = "git archive --format=tar --prefix=%(source)s-%(tagver)s/ %(tagver)s | xz > %(tempdir)s/%(source)s_%(tagver)s.orig.tar.xz" % {'source': self.pkgname, 'tagver': origin_ver, 'tempdir': temp_dir}
        with open(os.devnull, 'w') as devnull:
            subprocess.check_call(command, shell=True, cwd = self.source_cache, stdout=devnull, stderr=devnull)
            subprocess.check_call("tar xf %(source)s_%(tagver)s.orig.tar.xz" % {'source': self.pkgname, 'tagver': origin_ver}, shell=True, cwd = temp_dir)

        work_dir = os.path.join(temp_dir, "%s-%s" % (self.pkgname, origin_ver))
        if not os.path.exists(os.path.join(work_dir, 'debian')) or self.debian_url != self.source_url:
            with open(os.devnull, 'w') as devnull:
                debian_revision = self.get_release_version(url=self.debian_url, ref=self.debian_ref,  cwd=self.debian_cache)['sha']
                subprocess.check_call("git archive --format=tar --prefix=debian/ %s | (cd %s && tar xf -)" % (debian_revision, temp_dir), shell=True, cwd = self.debian_cache, stdout=devnull, stderr=devnull)

                _debian_dir = os.path.join(temp_dir, 'debian', 'debian')
                if not os.path.exists(_debian_dir):
                    raise IOError("debian path is not exists") 

                subprocess.check_call("rm -rf %s" % (os.path.join(work_dir, 'debian')), shell=True)
                subprocess.check_call("cp -r %s %s" % (_debian_dir, work_dir),shell=True)

        if not os.path.exists(os.path.join(work_dir, 'debian', 'source')):
            os.makedirs(os.path.join(work_dir, 'debian', 'source'))

        with open(os.path.join(work_dir, 'debian', 'source', 'format'), 'w') as fp:
            fp.write("3.0 (quilt)\n")

        try:
            contents = ['Auto Version update.']
            try:
                stdout = self.execute(['git', 'cat-file', 'tag', origin_ver] ,cwd = self.repo)
                add = False
                for line in stdout:
                    if line == "":
                        add = True
                        continue
                    if add:
                        contents.append(line)
            except Exception:
                pass

            binnum = 1
            while True:
                version = origin_ver + '-%d' % binnum
                try:
                    package = ShuttleJob.Package.selectBy(pkgname=self.pkgname, pkgver=version, reponame=reponame)

                except:
                    package = None
                if package and package.count():
                    binnum = binnum + 1
                else:
                    break
            with open(os.path.join(work_dir, 'debian', 'changelog.dch'), 'w') as cw:
                cp = {'Source': self.pkgname, 'Version': version, 'Distribution': dist, 'Urgency': urgency}
                cw.write("%(Source)s (%(Version)s) %(Distribution)s; urgency=%(Urgency)s\n" % cp)
                cw.write("\n")
                for line in contents:
                    cw.write("  * %s\n" % line)
                cw.write("\n")
                cw.write(" -- %s <%s>  %s\n" % (get_builder_name(), get_builder_email(), get_changelog_time()))
            os.rename(os.path.join(work_dir, 'debian', 'changelog.dch'), os.path.join(work_dir,  'debian', 'changelog'))
        except Exception as err:
            print(err)

        subprocess.check_call('dpkg-source -b %s' % work_dir, shell=True, cwd=temp_dir)
        files = []
        for d in os.listdir(temp_dir):
            if os.path.isfile(os.path.join(temp_dir, d)):
                files.append(d)

        return version, temp_dir, files, _revision

    def get_author(self, sha):
        try:
            author = self.execute(['git', 'show', '-s', '--format="%aN <%aE>"', sha])
        except Exception as e:
            author = None
        return author

    def tag(self, pkg_version=None, debian_ref=None, reponame=None, dist="unstable", urgency="low"):
        self.initial()
        temp_prefix = os.path.join("/tmp", "tag-archive-temp")
        if not os.path.exists(temp_prefix):
            os.makedirs(temp_prefix)
        if pkg_version is None:
            pkg_version = self.get_release_version(url=self.source_url, ref=self.source_ref, cwd=self.source_cache)['ver']

        temp_dir = tempfile.mkdtemp(dir=temp_prefix)
        try:
            _pkgver, _dir , _files, hashsum = self._tag(temp_dir=temp_dir, origin_ver=pkg_version, reponame=reponame, dist=dist, urgency=urgency)
        except Exception as e:
            os.system("rm -rf %s" % temp_dir)
            return {'version': None, 'message': str(e)}
        return {'version': _pkgver, 'path': _dir, 'files': _files, 'hashsum': hashsum }

if __name__ == "__main__":
    from  twisted.internet import reactor, defer, threads
    '''Config is a dict, like as
        {
            "source": "https://cr.deepin.io/dde/dde-daemon#branch=master",
            "debian": "https://cr.deepin.io/dde/dde-daemon#branch=debian",
            "builder": "default"
        }

    '''
    config = {"source": "https://cr.deepin.io/deepin-mutter#branch=release/3.20",
              "debian": "/srv/shuttle/repos/git/deepin-mutter#branch=master",
              "builder": "default"
              }
    g = GitBuilder(pkgname="deepin-mutter", config=config)
    result = g.archive()
    print(result)
    os.system("rm -rf %s" % result['path'])
    #t = threads.deferToThread(g.archive)
    #t.addCallback(lambda result: reactor.stop())
    #t.addErrback(lambda result: reactor.stop())
    #reactor.run()
