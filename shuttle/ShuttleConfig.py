#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

import ConfigParser
import os

class ShuttleConfig(object, ConfigParser.ConfigParser):
    config_file = "/etc/shuttle/shuttlerc"
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init(*args, **kwargs)
        return cls._instance

    def init(self, dontparse=False):
        ConfigParser.ConfigParser.__init__(self)
        self.add_section('http')
        self.add_section('source')
        self.add_section('build')
        self.add_section('notify')
        self.add_section('repos')
        self.add_section('log')

        self.set('http', 'port', '9998')
        self.set('http', 'ip', '0.0.0.0')
        self.set('http', 'logfile', '/var/log/shuttle/httpd.log')
        self.set('http', 'templates_dir', 'www/template')
        self.set('http', 'statics_dir', 'www/static')
        self.set('http', 'librarian', 'http://10.0.10.32/shuttle/')
        self.set('http', 'repos_url', 'http://10.0.10.32/shuttle/repos')

        self.set('build', 'check_every', '3')
        self.set('build', 'database_uri', 'sqlite:///srv/shuttle/shuttle.db')
        self.set('build', 'cachedir', '/srv/shuttle/cache')
        self.set('build', 'configdir', '/srv/shuttle/config/')
        self.set('build', 'basetgz', '/srv/shuttle/pbuilder/')

        self.set('source', 'cache_dir', '/srv/shuttle/git-repo')

        self.set('notify', 'enable', '1')
        self.set('notify', 'bearychat', '1')
        self.set('notify', 'bearychat_url', 'https://hook.bearychat.com/=bw5Bi/incoming/e35e89695fe08686f0671b6065fa2fdb')
        self.set('notify', 'bearychat_message', '${pkgname} - ${pkgver} to ${reponame} with ${dist} ${arch} [Failed](http://shuttle.corp.deepin.com/debian/job/${id}).')

        self.set('repos', 'basedir', '/srv/shuttle/repos')
        self.set('repos', 'config', '/srv/shuttle/config/repo')

        self.set('log', 'time_format', "%Y-%m-%d %H:%M:%S")
        self.set('log', 'file', "/tmp/shuttle.log")

        if not dontparse:
            self.reload()

    def reload(self):
        """ Reload configuration file """
        return self.read(self.config_file)

    def dump(self):
        """ Dump running configuration """
        conf = ""
        for section in self.sections():
            conf += "[" + section + "]\n"
            for item, value in self.items(section):
                conf += "%s = %s\n" % (item, value)
            conf += "\n"
        return conf

    def save(self):
        """ Save configuration file """
        try:
            self.write(file(self.config_file, 'w'))
        except Exception as error:
            print(error)
            return False
        return True
