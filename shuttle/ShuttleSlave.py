#!/usr/bin/env python

import xmlrpclib
from ShuttleJob import JobStatus, UploadStatus
from ShuttleJob import PbuildStatus
import ShuttleJob
from ShuttleConfig import ShuttleConfig
from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet.protocol import Protocol
from twisted.web import client
import functools
import json
import os
import gzip
import glob
import string

import ConfigParser

from ShuttleNotify import ShuttleNotify

from urlparse import urljoin

def urlappend(baseurl, path):
    assert not path.startswith('/')
    if not baseurl.endswith('/'):
        baseurl += '/'
    return urljoin(baseurl, path)

class TimeoutTransport(xmlrpclib.Transport):
    def __init__(self, timeout, use_datetime=0):
        self.timeout = timeout
        # xmlrpclib uses old-style classes so we cannot use super()
        xmlrpclib.Transport.__init__(self, use_datetime)

    def make_connection(self, host):
        connection = xmlrpclib.Transport.make_connection(self, host)
        connection.timeout = self.timeout
        return connection


class TimeoutServerProxy(xmlrpclib.ServerProxy):
    def __init__(self, uri, timeout=10, transport=None, encoding=None, verbose=0, allow_none=0, use_datetime=0):
        t = TimeoutTransport(timeout)
        xmlrpclib.ServerProxy.__init__(self, uri, t, encoding, verbose, allow_none, use_datetime)


class FileWritingQueue():
    def __init__(self):
        self.work = False
        self.tasks = []
        self.drain = None

    def download(self, data):
        url = data[0]
        filename = data[1]
        basepath = os.path.dirname(filename)
        if not os.path.exists(basepath):
            os.makedirs(basepath)
        d = client.downloadPage(url, filename, timeout=300)
        return d

    def push(self, data):
        self.tasks.append(data)
        self.process()

    def task_finished(self, *args):
        self.work =  False
        if len(self.tasks) == 0 and self.drain:
            self.drain()
        self.process()

    def process(self):
        if len(self.tasks) != 0 and self.work:
            return
        task = self.tasks.pop(0)
        self.work = True
        d = self.download(task)
        d.addBoth(self.task_finished)

class ShuttleSlaveConfig(object, ConfigParser.ConfigParser):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init(*args, **kwargs)
        return  cls._instance

    def init(self):
        #TODO: need fix config file patch 
        self.config_file = os.path.join(ShuttleConfig().get("build","configdir"), "slaves.conf")
        ConfigParser.ConfigParser.__init__(self)
        self.reload()

    def reload(self):
        return self.read(self.config_file)

    def save(self):
        try:
            self.write(file(self.config_file, "w"))
        except Exception as err:
            return False
        return True

    def slaves(self):
        slaves = []
        for slave in self.sections():
            slave_rpc = self.get(slave, "rpc")
            slave_method = self.get(slave, "method")
            if slave_method == "debian-package":
                slave = ShuttleDebianPackageSlave(slave, slave_rpc)
            elif slave_method == "pbuilder-image":
                slave = ShuttlePbuildImageSlave(slave, slave_rpc)
            else:
                raise NotImplementedError("%s is not implemented" % slave_method)
            slaves.append(slave)
        return slaves

    @property
    def list_slaves(self):
        slaves = []
        for slave in self.sections():
            slaves.append(slave)
        return slaves

    def add_slave(self, slave):
        if slave in self.list_slaves:
            return 
        self.add_section(slave)
        self.save()
        self.reload()

    def remove_slave(self, slave):
        if slave in self.list_salves:
            self.remove_section(slave)
            self.save()
            self.reload()


class ShuttleSlave(object):
    def __init__(self, name, builder_url):
        self.name = name
        self.builder_url = builder_url
        self.rpc  = urlappend(builder_url, 'rpc')
        self._file_cache_url = urlappend(builder_url, 'filecache')
        self.proxy = TimeoutServerProxy(self.rpc, 1)
        self.cachedir = ShuttleConfig().get("build", "cachedir")
        self.configdir = ShuttleConfig().get("build","configdir")
        self.librarian = ShuttleConfig().get("http", "librarian")
        self.uploading = False
        self.refresh()

    def refresh(self):
        try:
            info = self.proxy.info()
        except Exception as err:
            info = {}

        self.dists = info.get("dists", [])
        self.arches = info.get("arches", [])
        self.builders = info.get("builders", [])

    def getFiles(self, files, basepath, id):
        self.uploading = True
        queue = FileWritingQueue()
        queue.drain = functools.partial(self.upload_done, id)
        for file in files:
            url = urlappend(self._file_cache_url, file)
            save = os.path.join(basepath, file)
            queue.push([url, save])

    def status(self):
        try:
            return self.proxy.status()
        except Exception as err:
            return {'builder_status': 'BuilderStatus.OFFLINE', 'error': err}

    def support(self, builder):
        if builder in self.builders:
            return True
        return False

    def build(self, buildid, builder, *args):
        raise NotImplementedError("Should be subclassed to be used")

    def complete(self):
        raise NotImplementedError("Should be subclassed to be used")

    def upload_done(self, *args):
        raise NotImplementedError("Should be subclassed to be used")


class ShuttleDebianPackageSlave(ShuttleSlave):
    def upload_done(self, jobid):
        self.uploading = False
        self.proxy.clean()
        job = ShuttleJob.Job.selectBy(id=jobid)[0]
        package = job.package

        for _job in ShuttleJob.Job.selectBy(package=package):
            all_done = True
            if _job.status != JobStatus.BUILD_OK:
                all_done = False
                break
        if all_done:
            package.upload_status = UploadStatus.WAIT

        try:
            if ShuttleNotify().notify_method.get("bearychat", None) is not None:
                if job.status != JobStatus.BUILD_OK:
                    from ShuttleLog import DebianLogParser
                    resultdir = "%s-%s" % (job.dist, job.arch)
                    basepath  = os.path.join(self.cachedir, 'debian-package', str(package.id),  resultdir)

                    message_template = string.Template(ShuttleConfig().get('notify', 'bearychat_message'))

                    message = message_template.substitute({
                        "reponame": package.reponame,
                        "pkgname": package.pkgname,
                        "pkgver": package.pkgver,
                        "dist": job.dist,
                        "arch": job.arch,
                        "id": job.id
                        })
                    attachments = {
                        "text": DebianLogParser(basepath).parser_buildlog(),
                        "color": "#ffa500"
                        }
                    ShuttleNotify().notify("bearychat", message, message_attachments=[ attachments ])
        except Exception as e:
            print(e)
            pass


    def build(self, buildid, builder, name, version, dist, arch):
        job = ShuttleJob.Job.selectBy(id=buildid)[0]
        reponame = job.package.reponame
        files  =  ["%s_%s.dsc" % (name, version)]
        configdir = os.path.join(self.configdir, 'package', reponame)
        if not os.path.exists(configdir):
            configdir = os.path.join(self.configdir, 'package', 'defaults')

        default_config = os.path.join(configdir, "default.json")
        specify_config = os.path.join(configdir, "%s.json"  % name)

        resultdir = "%s-%s" % (job.dist, job.arch)
        log_path = os.path.join(self.cachedir, 'debian-package', str(job.package.id), resultdir, 'buildlog')
        if not os.path.exists(os.path.dirname(log_path)):
            os.makedirs(os.path.dirname(log_path))

        if not os.path.exists(default_config):
            with open(log_path, "w") as fp:
                fp.write("Opps, Something wrong! Job will be Given up.\n")
                fp.write("%s is not exists\n" % default_config)
            job.status = JobStatus.CANCELED
            return

        extra_args = json.load(open(default_config, "r"))
        if os.path.exists(specify_config):
            extra_args.update(json.load(open(specify_config, "r")))

        binnmu = int(job.package.binnmu)

        #Support BINNMU version
        if binnmu != 0:
            extra_args['binnmu'] = "yes"
            extra_args['binnmu_version'] = binnmu
            dep_pkg = [ dep.pkgname for dep in job.package.deps ]
            extra_args['binnmu_message'] = "Auto rebuild with package update: %s" % " ".join(dep_pkg)
        else:
            extra_args['binnmu'] = "no"

        repo_config = os.path.join(self.configdir, "repo", "%s.json" % reponame)
        if not os.path.exists(repo_config):
            with open(log_path, "w") as fp:
                fp.write("Opps, Something wrong!\n")
                fp.write("%s is not exists\n" % repo_config)
            job.status = JobStatus.CANCELED
            return

        _config = json.load(open(repo_config, "r"))

        if extra_args.get('archives', None) is None:
            extra_args['archives'] = []

        repo_url = _config.get("url", None)
        if repo_url:
            apt_line = "deb %s %s main" % (repo_url, dist)
            extra_args['archives'].append(apt_line)

        add_suffix = _config.get("add_suffix", "0")
        if add_suffix == "1":
            extra_args['add_suffix'] = dist
            
        for apt_line in _config.get("archives"):
            extra_args['archives'].append(apt_line.replace("distribution", dist))
        if _config.get("without_builder_cache", "0") == "1":
            # we may not use pbuilder cache but have cache saved at slave
            # and you should config with without_builder_cache in repo config
            extra_args.update({'dist': dist, 'arch': arch, 'basetgz': 'no'})
        else:
            field = _config.get("base")
            pbuilder_cache = ShuttleConfig().get("build", "basetgz")

            if dist == 'experimental':
                dist = 'unstable'

            # if field build cache not exists, fallback to default
            builder_json =  os.path.join(pbuilder_cache, field, "%s.%s.%s" % (extra_args['builder'], dist, arch), "result.json")
            if not os.path.exists(builder_json):
                extra_args['builder'] = 'default'
                builder_json = os.path.join(pbuilder_cache, field, 'default.%s.%s' % (dist, arch), "result.json")

            if os.path.exists(builder_json):
                extra_args['basetgz'] = os.path.join(field, "%s.%s.%s" %(extra_args['builder'], dist, arch), 'base.tgz') 
                try:
                    extra_args.update(json.load(open(builder_json,"r")))
                    #The result.json dist and arch may not corrent.
                    extra_args.update({'dist': dist, 'arch': arch})
                except Exception as e:
                    print(e)
                    job.status = JobStatus.GIVEUP
                    return False
            else:
                print("builder config: %s is not exists.We will skip it." % builder_json)
                job.status = JobStatus.GIVEUP
                return False

        extra_args['librarian'] = self.librarian
        extra_args['source_id'] = str(job.package.id)

        return self.proxy.build(buildid, builder, files, extra_args)

    def complete(self):
        status = self.proxy.status()
        files = []
        buildid = status.get("build_id")
        job = ShuttleJob.Job.selectBy(id=buildid)[0]

        resultdir = "%s-%s" % (job.dist, job.arch)

        results = status.get("filemap", {})
        for file in results:
            files.append(file)
        basepath = os.path.join(self.cachedir, 'debian-package', str(job.package.id),  resultdir)
        files.append('buildlog')
        d = self.getFiles(files=files, basepath=basepath, id=buildid)
        return d

class ShuttlePbuildImageSlave(ShuttleSlave):
    def upload_done(self, jobid):
        try:
            job = ShuttleJob.Pbuild.selectBy(id=jobid)[0]
            if job.status == PbuildStatus.BUILD_OK:
                new_path = os.path.join(self.cachedir, 'pbuilder', str(jobid))
                linkname  = "%s.%s.%s" % (job.jobname, job.dist, job.arch)
                linkpath = os.path.join(ShuttleConfig().get("build", "basetgz"), job.field, linkname)

                if not os.path.exists(os.path.dirname(linkpath)):
                    os.makedirs(os.path.dirname(linkpath))

                if os.path.exists(linkpath) and os.path.islink(linkpath):
                    old_path = os.path.realpath(linkpath)
                else:
                    old_path = None

                os.system("rm -f %s" % linkpath)
                os.system("ln -sf %s %s" % (new_path, linkpath))
                # Remove some old archive to save disk space
                if old_path is not None:
                    remove_item = ["base.tgz"]
                    for item in remove_item:
                        item_path = os.path.join(old_path, item)
                        if os.path.exists(item_path):
                            os.system("rm -f %s" % item_path)
        finally:
            self.uploading = False
            self.proxy.clean()

    def build(self, buildid, builder, field, name, dist, arch):
        configdir = os.path.join(self.configdir, 'builder')
        config = "%s.json" % name
        if not os.path.exists(os.path.join(configdir, field, config)):
            job = ShuttleJob.Pbuild.selectBy(id=jobid)[0]
            job.status =  PbuildStatus.CONFIG_FAILED
            raise OSError("%s not exists" % config)

        librarian = urlappend(self.librarian, 'config/builder/%s' % field)

        extra_args = {'dist': dist, 'arch': arch, 'librarian': librarian}
        return self.proxy.build(buildid, builder, config, extra_args)

    def complete(self):
        status = self.proxy.status()
        files = []
        buildid = status.get("build_id")
        job = ShuttleJob.Pbuild.selectBy(id=buildid)[0]

        results = status.get("filemap", {})
        for file in results:
            files.append(file)
        files.append('buildlog')
        basepath = os.path.join(self.cachedir, 'pbuilder', str(job.id))
        d = self.getFiles(files=files, basepath=basepath, id=job.id)
        return d
