#!/usr/bin/env python

import threading
from ShuttleLog import ShuttleLog
import sqlobject
from ShuttleJob import JobStatus, JobFailedStatus, UploadStatus
from ShuttleConfig import ShuttleConfig
from ShuttleSlave import ShuttleSlave 
import ShuttleJob
import signal
import sys
import glob
from datetime import datetime
import json
import socket
import os

class Shuttle(object):
    slaves = []
    jobs = []
    _instance = None
    supports = ['arch', 'debian', 'ubuntu']

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init()
        return cls._instance

    def init(self):
        self.cfg = ShuttleConfig()
        ShuttleLog()
        self._sqlconnection = sqlobject.connectionForURI(self.cfg.get('build', 'database_uri'))
        sqlobject.sqlhub.processConnection = self._sqlconnection

        self.jobs_locker = threading.Lock()
        self.do_quit = threading.Event()

    def _loop(self):
        self.flog_slaves()
        self.start_jobs()
        self.fix_jobs()
        self.clean_jobs()
        self.do_quit.wait(1)

    def loop(self):
        counter = self.cfg.getint('build', 'check_every')
        while not self.do_quit.isSet():
            if counter == self.cfg.getint('build', 'check_every'):
                self.start_jobs()
                self.fix_jobs()
                self.clean_jobs()
                counter = 0
            self.do_quit.wait(1)
            counter += 1

    def daemon(self):
        ShuttleLog.info("Starting Shuttle")
        self.daemonize()

        ShuttleLog.info("Run main loop")
        self.loop()

        ShuttleLog.info("Release wait locked jobs")
        self.release_jobs()

        ShuttleLog.info("Exiting rebuildd")

    def daemonize(self):
        signal.signal(signal.SIGTERM, self.handle_sigterm)
        signal.signal(signal.SIGINT, self.handle_sigterm)
        try:
            sys.stdout = sys.stderr = open(self.cfg.get('log', 'file'), 'a')
        except Exception as error:
            print("E: unable to open logfile: %s" % error)


    def release_jobs(self):
        with self.jobs_locker:
            for job in self.jobs:
                with job.status_lock:
                    if job.status == JobStatus.WAIT_LOCKED:
                        job.status = JobStatus.WAIT
                        job.host = ""
        return True


    def add_slave(self, slave):
        for _slave in self.slaves:
            if _slave.name == slave.name:
                ShuttleLog.warn("Slave %s is already exists" % slave.name)
                return
        self.slaves.append(slave)

    def add_job(self, worker, name, version, hashsum=None, priority=None, dists=[], arches=[]):
        if not arches:
            arches = [self.cfg.arch]

        if worker not in self.supports:
            ShuttleLog.error("Worker: %s is not supported" % worker)
            raise ValueError("Worker is not supported")

        if worker == "debian":
            pkgs = ShuttleJob.Package.selectBy(pkgname=name, pkgver=version)
            if pkgs.count():
                pkg = pkgs[0]
            else:
                pkg = ShuttleJob.Package(name=name, version=version, priority=priority, hashsum=hashsum)

            if len(pkg.jobs) == 0:
                for dist in dists:
                    for arch in arches:
                        _job = ShuttleJob.Job(dist=dist, arch=arch, package=pkg)
                        _job.status = JobStatus.WAIT
            else:
                for dist in dists:
                    for arch in arches:
                        for job in pkg.jobs:
                            if job.dist == dist and job.arch == arch:
                                ShuttleLog.error("Job already exists for %s-%s, not adding it" % (pkg.name, pkg.version))
                                continue
                        _job = ShuttleJob.Job(dist=dist, arch=arch, package=pkg)
                        _job.status = WAIT


    def get_all_packages(self, **kwargs):
        packages = []
        packages.extend(ShuttleJob.Package.selectBy(**kwargs).orderBy("-id"))
        return packages

    def get_all_jobs(self, **kwargs):
        jobs = []
        jobs.extend(ShuttleJob.Job.selectBy(**kwargs))
        return jobs

    def status(self):
        waiting = ShuttleJob.Job.selectBy(status=JobStatus.WAIT).count()
        building = ShuttleJob.Job.selectBy(status=JobStatus.BUILDING).count()
        return {'waiting': waiting, 'building': building }

    def flog_slaves(self):
        for slave in self.slaves:
            status = slave.status()
            if status.get('builder_status', None) == 'BuilderStatus.OFFLINE':
                #ShuttleLog.warn("Slave: %s is offline, remove it by: %s"  % (slave.name, status.get('error'))) 
                #self.slaves.remove(slave)
                continue

            if status.get('builder_status', None) == 'BuilderStatus.ABORTING':
                buildid = status.get('build_id', None)
                job = self.get_all_jobs(id=buildid)[0]
                job.status = JobStatus.CANCELED
                slave.complete()

            if status.get('builder_status', None) == 'BuilderStatus.WAITING' and slave.uploading is False:
                buildid = status.get('build_id', None)
                if buildid is None:
                    continue
                jobs = self.get_all_jobs(id=buildid)
                if len(jobs) == 0:
                    ShuttleLog.warn('Job: %s cannot get job instance' % buildid)
                else:
                    job = jobs[0]
                    if status.get('build_status') == "BuildStatus.OK":
                        ShuttleLog.info("Set %s %s %s:%s build status: %s" % (job.package.pkgname, job.package.pkgver, job.dist, job.arch, JobStatus.whatis(JobStatus.BUILD_OK)))
                        job.status = JobStatus.BUILD_OK
                    else:
                        ShuttleLog.info("Set %s %s %s:%s build status: %s" % (job.package.pkgname, job.package.pkgver, job.dist, job.arch, JobStatus.whatis(JobStatus.FAILED)))
                        job.status = JobStatus.FAILED
                    slave.complete()
            slave.refresh()

    def start_jobs(self):
        with self.jobs_locker:
            # skip old version build
            for job in ShuttleJob.Job.selectBy(status=JobStatus.WAIT):
                try:
                    package = ShuttleJob.Package.selectBy(id=job.package.id)[0]
                except Exception as e:
                    print(e)
                    job.status = JobStatus.GIVEUP
                    continue

                candidate_packages = self.get_all_packages(pkgname=package.pkgname, reponame=package.reponame, upload_status=UploadStatus.UNKNOWN)
                candidate_packages.sort(cmp=ShuttleJob.Package.version_compare)

                newpackage = None

                for cpackage in candidate_packages:
                    if newpackage and newpackage != cpackage and cpackage.upload_status == UploadStatus.UNKNOWN:
                        ShuttleLog.info("Set build %d:%s-%s to giveup build" % (cpackage.id, cpackage.pkgname, cpackage.pkgver))
                        cpackage.giveup()
                    elif cpackage.upload_status == UploadStatus.UNKNOWN:
                        newpackage = cpackage

            for slave in self.slaves:
                status = slave.status()
                if status.get('builder_status', None) != 'BuilderStatus.IDLE':
                    continue

                for job in ShuttleJob.Job.selectBy(status=JobStatus.WAIT):
                    package = ShuttleJob.Package.selectBy(id=job.package.id)[0]
                    if not package.is_allowed_to_build():
                        continue
                    if job.dist in slave.dists and job.arch in slave.arches:
                        if job in self.jobs:
                            break
                        job.status = JobStatus.WAIT_LOCKED
                        ShuttleLog.info("Launch %s to build %s:%s %s" % (slave.name, job.package.pkgname, job.package.pkgver, job.arch))
                        try:
                            job.start(slave, 'debian')
                        except Exception as e:
                            job.status = JobStatus.WAIT
                            ShuttleLog.warn("%s exception: %s" % (slave.name, e))
                        else:
                            self.jobs.append(job)
                        break

    def fix_jobs(self):
        with self.jobs_locker:
            for job in ShuttleJob.Job.selectBy(status=JobStatus.BUILDING):
                for slave in self.slaves:
                    if slave.name == job.build_host:
                        if slave.status().get('build_id', None) != job.id:
                            job.status = JobStatus.WAIT

    def clean_jobs(self):
        for job in self.jobs:
            if job.status >= JobStatus.SOURCE_FAILED:
                job.build_end = sqlobject.DateTimeCol.now()
                self.jobs.remove(job)

    def upload_packages(self):
        pass


    def handle_sigterm(self, signum, stack):
        ShuttleLog.info("Receiving transmission... it's a signal %s capt'ain! EVERYONE OUT!" % signum) 
        self.do_quit.set()

if __name__ ==  "__main__":
    shuttle = Shuttle()
    shuttle.cfg.set("build",  "check_every",  "3")
    slave = ShuttleSlave('vvvvvv', 'debian')
    shuttle.add_slave(slave)
    shuttle.loop()
