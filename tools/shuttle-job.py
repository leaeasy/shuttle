#!/usr/bin/env python
import glob
import os
import subprocess
import datetime
import sys
import json
sys.path.append('.')
from shuttle.Shuttle import Shuttle
try:
    from shuttle.ShuttleJob import JobStatus, PbuildStatus, UploadStatus
    from shuttle import ShuttleJob
    from shuttle.ShuttleConfig import ShuttleConfig
    from shuttle.ShuttleSlave import ShuttleSlaveConfig
    from shuttle.ShuttleSlave import ShuttleDebianPackageSlave
except:
    raise

Shuttle()

class DscFile(object):
    def __init__(self, dscfile):
        self.archs = []
        self.files = []
        self.dscfile = dscfile
        self.basepath = os.path.dirname(os.path.abspath(self.dscfile))
        self.init()

    def init(self):
        fileflag = False
        self.version = None
        with open(self.dscfile) as fp:
            for line in fp.readlines():
                if line.endswith('\n'):
                    line = line[:-1]
                if line.startswith('Source: '):
                    self.source = line.split(':')[1].strip()
                    continue
                if line.startswith('Version: '):
                    if self.version is None:
                        self.version = line.split(':')[1].strip()
                    continue
                if line.startswith('Architecture: '):
                    self.archs = line.split(':')[1].strip().split(',')
                    continue
                if not fileflag and line.startswith('Files:'):
                    fileflag = True
                elif fileflag:
                    if not line.startswith(' '):
                        fileflag = False
                    else:
                        filename = line.split(' ')[-1]
                        self.files.append(filename)
        self.files.append(os.path.basename(self.dscfile))

    def echo(self):
        print(self.source, self.version, self.archs, self.files, self.basepath)


def add_dsc(dscfile, reponame='debian', arches=['amd64']):
    dsc = DscFile(dscfile)
    dsc.echo()

    package = ShuttleJob.Package(pkgname=dsc.source, pkgver=dsc.version, reponame=reponame, action="release")
    dest_dir = os.path.join(ShuttleConfig().get("build", "cachedir"), "debian-package", str(package.id), 'source')

    for file in dsc.files:
        print("Add file to source: %s" % file)
        os.system("install -Dm644 %s %s" %(os.path.join(dsc.basepath, file), os.path.join(dest_dir, file)))

    for arch in arches:
        ShuttleJob.Job(arch=arch, package=package, status=JobStatus.WAIT)

def requeue(pkgid):
    if ShuttleJob.Package.selectBy(id=pkgid).count() == 0:
        return

    pkg = ShuttleJob.Package.selectBy(id=pkgid)[0]
    pkg.upload_status = UploadStatus.UNKNOWN
    for job in pkg.jobs:
        job.status = JobStatus.WAIT

def abort(jobid):
    if ShuttleJob.Job.selectBy(id=jobid).count() == 0:
        return

    job = ShuttleJob.Job.selectBy(id=jobid)[0]
    if job.status != JobStatus.BUILDING:
        print("Job status is not BUILDING")
        return
    for slave in ShuttleSlaveConfig().slaves():
        if job.build_host  == slave.name:
            print(slave.name, slave.proxy)
            status = slave.status()
            slave.proxy.abort()
            break
        

def upload(pkgid):
    if ShuttleJob.Package.selectBy(id=pkgid).count() == 0:
        return

    pkg = ShuttleJob.Package.selectBy(id=pkgid)[0]

    for job in pkg.jobs:
        if job.status != JobStatus.BUILD_OK:
            print("Job should be build_ok")
            return

    pkg.upload_status = UploadStatus.WAIT

def delete_package(pkgid):
    if ShuttleJob.Package.selectBy(id=pkgid).count() == 0:
        print("pkgid %d not exists")
        return
    pkg=ShuttleJob.Package.selectBy(id=pkgid)[0]
    print("Delete %d: %s - %s" % (pkg.id, pkg.pkgname, pkg.pkgver))
    for job in pkg.jobs:
        print("  deleting job id: %d" % job.id)
        job.destroySelf()
    pkg.destroySelf()


def release(pkgname, reponame, arches=['amd64', 'i386']):
    from shuttle.ShuttleGit import GitBuilder
    configdir = ShuttleConfig().get("build", "configdir")
    pkgconfig = os.path.join(configdir, "package", reponame, "%s.json" % pkgname)
    if not os.path.exists(pkgconfig):
        print("%s is not exists" % pkgconfig)
        return
    config = json.load(open(pkgconfig, "r"))
    g = GitBuilder(pkgname=pkgname, config=config)
    g.initial()

    version = g.get_release_version(url=g.source_url, ref=g.source_ref, cwd=g.source_cache)['ver'] + "-1"
    print("Release version: %s" % version)

    if ShuttleJob.Package.selectBy(pkgname=pkgname, pkgver=version, reponame=reponame).count() != 0:
        package = ShuttleJob.Package.selectBy(pkgname=pkgname, pkgver=version, reponame=reponame)[0]
        print("Please requeue package id: %d" % package.id)
        print(" -> %s: %s" % (package.pkgname, package.pkgver))
        return

    result = g.tag()
    print(result)

    if result.get('version', None):
        dscfile = None
        try:
            for f in result['files']:
                if f.endswith(".dsc"):
                    dscfile = os.path.join(result['path'], f)
                    break
            if dscfile is not None:
                add_dsc(dscfile, reponame=reponame, arches=arches)
            else:
                print("No dsc file generate!")
        finally:
            if result.get("path", None):
                os.system("rm -rf %s" %  result['path'])

def pbuilder_nightly(field, bases=[], arches=['amd64', 'i386']):
    for base in bases:
        for arch in arches:
            ShuttleJob.Pbuild(field=field, jobname=base, dist='unstable', arch=arch, status=PbuildStatus.WAIT)

def show_help():
    print("Args: add-dsc | requeue | abort | release | upload | add | delete")

if __name__ == "__main__":
    if sys.argv[1] == "add-dsc":
        add_dsc(sys.argv[2])
    if sys.argv[1] == "refresh-pbuilder":
        if sys.argv[2] == 'deepin':
            pbuilder_nightly('deepin', bases=['default', 'qt5', 'go'])
        if sys.argv[2] == 'debian':
            pbuilder_nightly('debian', bases=['default'])
    if sys.argv[1] == "requeue":
        requeue(sys.argv[2])
    if sys.argv[1] == "abort":
        abort(sys.argv[2])
    if sys.argv[1] == "release":
        release(sys.argv[2], 'debian', ['amd64'])
    if sys.argv[1] == "upload":
        upload(sys.argv[2])
    if sys.argv[1] == "add":
        add_package(sys.argv[2], sys.argv[3], "debian")
    if sys.argv[1] == 'delete':
        delete_package(sys.argv[2])
