#!/usr/bin/env python

CACHE_DIR="/tmp/git-repo"
SOURCE_DIR="/tmp/sources"
import os
import subprocess
import tempfile
import time
from threading import Timer

from twisted.internet import reactor, defer, threads

class Dch():
    def __init__(self, debian_repo):
        self.debian_repo = debian_repo

    def get_builder_name(self, default="shuttle"):
        return os.environ.get("DEBFULLNAME", default)

    def get_builder_email(self, default="shuttle@deepin.com"):
        return os.environ.get("DEBEMAIL", default)

    def get_changelog_time(self):
        return time.strftime("%a, %d %b %Y %T %z", time.localtime())

    def mangle_changelog(self, cp, revision, branch, content):
        try:
            with open(os.path.join(self.debian_repo, 'debian', 'changelog.dch'), 'w') as cw:
                cw.write("%(Source)s (%(Version)s) %(Distribution)s; urgency=%(Urgency)s\n" % cp)
                cw.write("\n")
                cw.write("  ** Build revesion: %s @ %s **\n\n" % (revision, branch))

                if isinstance(content, str):
                    cw.write("  * %s\n" % content)
                    cw.write("\n")
                if isinstance(content, list):
                    cw.write("  * detail lastest %d commit logs below: \n" % (len(content)))
                    for i in  content:
                        cw.write("   - %s\n" % i)
                    cw.write("\n")
                if isinstance(content, dict):
                    for key in content:
                        cw.write("  [%s]\n" % i)
                        for value in content.get(key):
                            cw.write("  * %s\n" % value)
                        cw.write("\n")
                    cw.write("\n")
                cw.write(" -- %s <%s>  %s\n" % (self.get_builder_name(), self.get_builder_email(), self.get_changelog_time()))
            os.rename(os.path.join(self.debian_repo, 'debian', 'changelog.dch'), os.path.join(self.debian_repo,  'debian', 'changelog'))
        except Exception as err:
            print(err)
            return False
        return True


class GitBuilder():
    def __init__(self, name, remote_url=None):
        self.name = name
        self.remote_url = remote_url
        self.repo = os.path.join(CACHE_DIR, self.name)

    def execute(self, subprocess_args, cwd=None, extended_output=False, timeout=600):
        if cwd is None:
            cwd =  self.repo

        env = os.environ.copy()
        env["LC_MESSAGE"] = "C"
        if isinstance(subprocess_args, str):
            subprocess_args = subprocess_args.split()
        execute_cmd = list(subprocess_args)
        proc = subprocess.Popen(list(subprocess_args), env=env, cwd=cwd,  stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=None)

        kill_proc = lambda p: p.kill()
        timer = Timer(timeout, proc.kill)
        timer.start()
        try:
            stdout, stderr = proc.communicate()
            status = proc.returncode
            if stdout.endswith('\n'):
                stdout = stdout[:-1]
            if stderr.endswith('\n'):
                stderr = stdout[:-1]
        finally:
            proc.stdout.close()
            proc.stderr.close()

        if timer.is_alive():
            timer.cancel()


        if status != 0:
            raise IOError("Subprocess  \"%s\" Error: %d at %s\nError: %s"  % (" ".join(list(subprocess_args)), status, cwd, stderr))
        
        if extended_output:
            return (status, stdout, stderr)
        else:
            return stdout

    def initial(self):
        return self.execute(['git', 'clone', '--bare', self.remote_url, self.name ], cwd = CACHE_DIR)

    def update(self):
        if not os.path.exists(self.repo):
            self.initial()
        return self.execute(['git', 'fetch', '-pfu' ])


    def tagver(self, branch):
        curtag = self.execute(['git', 'rev-list', '--tags', '--max-count=1', branch])
        tagver = self.execute(['git', 'describe',  '--tags',  curtag])
        return tagver


    def pkgver(self, branch="master", fallback_version="0.0"):
        try:
            stdout = self.execute(['git', 'describe', '--tags', '--long', branch])
            [_, rev, sha] = stdout.split("-")[:3]
            ver =  self.tagver(branch=branch)
        except:
            ver = fallback_version
            rev = self.execute(['git', 'rev-list',  '--count', branch])
            sha = self.execute(['git', 'rev-parse',  '--short', branch])

        return {"ver":ver, "rev":rev,  "sha":sha}

    def log(self, branch, maxlog=5):
        logs = self.execute(['git', 'log', "--pretty='%s'", "-%d" % maxlog, branch])
        result = []
        for log in  logs.split('\n'):
            if log.startswith("'") and log.endswith("'"):
                log = log[1:-1]
            result.append(log)
        return result

    def _archive(self, orig_branch, debian_branch, max_orig_changes, temp_dir):
        pkgver = self.pkgver(orig_branch)
        _revision = pkgver['sha']
        _pkgver = "%(ver)s+r%(rev)s~%(sha)s" % pkgver
        _prefix = "%s-%s/" % (self.name, _pkgver)

        with open(os.devnull, 'w') as devnull:
            subprocess.check_call("git archive --format=tar --prefix=%s %s | (cd %s && tar xvf -)" % (_prefix, _revision, temp_dir), shell=True, cwd = self.repo, stdout=devnull, stderr=devnull)

        if debian_branch is not None:
            debian = self.pkgver(debian_branch)
            with open(os.devnull, 'w') as devnull:
                subprocess.check_call("git archive --format=tar --prefix=debian/ %s | (cd %s && tar xvf -)" % (debian['sha'], temp_dir), shell=True, cwd = self.repo, stdout=devnull, stderr=devnull)
            _debian_dir = os.path.join(temp_dir, 'debian')
            subprocess.check_call("rm -rf %s" % (os.path.join(temp_dir, _prefix, 'debian')), shell=True)
            subprocess.check_call("mv %s %s" % (os.path.join(temp_dir, 'debian', 'debian'), os.path.join(temp_dir, _prefix)),shell=True)

        work_dir = os.path.join(temp_dir, _prefix)

        if not os.path.exists(os.path.join(work_dir, 'debian', 'source')):
            os.makedirs(os.path.join(work_dir, 'debian', 'source'))

        with open(os.path.join(work_dir, 'debian', 'source', 'format'), 'w') as fp:
            fp.write("3.0  (native)\n")

        sha = self.execute(['git', 'rev-parse', orig_branch])
        content = self.log(branch=orig_branch, maxlog=max_orig_changes)

        dch =  Dch(os.path.join(work_dir))
        cp = {'Source': self.name, 'Version': _pkgver, 'Distribution': 'UNRELEASED', 'Urgency': 'low' }
        dch.mangle_changelog(cp=cp, revision=sha, branch=orig_branch, content=content)

        subprocess.check_call('dpkg-source -b %s' % _prefix, shell=True, cwd=temp_dir)
        files = []
        for d in os.listdir(temp_dir):
            if os.path.isfile(os.path.join(temp_dir, d)):
                files.append(d)

        return temp_dir, files, _pkgver

    def archive(self, orig_branch="master", debian_branch="debian", max_orig_changes=1):
        self.update()
        temp_prefix = os.path.join("/tmp", "%s-temp" % self.name)
        if not os.path.exists(temp_prefix):
            os.makedirs(temp_prefix)
        temp_dir =  tempfile.mkdtemp(dir=temp_prefix)
        try:
            _dir, _files, _pkgver = self._archive(orig_branch, debian_branch, max_orig_changes, temp_dir)
            if not os.path.exists(os.path.join(SOURCE_DIR, self.name)):
                os.makedirs(os.path.join(SOURCE_DIR, self.name))
            for _file in _files:
                subprocess.check_call("cp %s %s" % (os.path.join(_dir, _file), os.path.join(SOURCE_DIR, self.name)), shell=True)
        except Exception as e:
            raise IOError("Error...")
        finally:
            os.system("rm -rf %s" % temp_dir)

        return _pkgver

if __name__  ==  "__main__":
    for dir in [CACHE_DIR, SOURCE_DIR]:
        if not os.path.exists(dir):
            os.makedirs(dir)
    g = GitBuilder(name="atm-sqfs2patch", remote_url="http://10.0.1.204:3000/lilongyu/atm-sqfs2patch.git")
    t = threads.deferToThread(g.archive, 'master', 'master-debian',5 )
    t.addCallback(lambda result: reactor.stop())
    t.addErrback(lambda result: reactor.stop())
    reactor.run()
