import sys
sys.path.append('..')
from crontab import CronTab
from twisted.internet import reactor
monday_morning = CronTab("0 8 * * 1")

def do_something():
    reactor.callLater(monday_morning.next(), do_something)
    # do whatever you want!

reactor.callLater(monday_morning.next(), do_something)
reactor.run()
