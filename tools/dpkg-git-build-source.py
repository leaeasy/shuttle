#!/usr/bin/env python
import argparse
import sys
import subprocess
from threading import Timer
import time
import os
import shutil
import tempfile

def get_builder_name(default="shuttle"):
    return os.environ.get("DEBFULLNAME", default)

def get_builder_email(default="shuttle@deepin.com"):
    return os.environ.get("DEBEMAIL", default)

def get_changelog_time():
    return time.strftime("%a, %d %b %Y %T %z", time.localtime())



class Dch():
    def __init__(self, debian_path):
        self.debian_path = debian_path

    def mangle_changelog(self, cp, revision=None, content="No changelog provides"):
        try:
            with open(os.path.join(self.debian_path, 'debian', 'changelog.dch'), 'w') as cw:
                cw.write("%(Source)s (%(Version)s) %(Distribution)s; urgency=%(Urgency)s\n" % cp)
                cw.write("\n")
                if revision:
                    cw.write("  ** Build revision: %s **\n\n" % revision)

                if isinstance(content, str):
                    cw.write("  * %s\n" % content)
                if isinstance(content, list):
                    cw.write("  * Detail changelog %d changes below: \n" % (len(content)))
                    for c in content:
                        cw.write("   - %s\n" % c)
                if isinstance(content, dict):
                    for key in content:
                        cw.write("  [%s]\n" % key)
                        values = content.get(key)
                        if isinstance(values, str):
                            cw.write("  * %s\n" % values)
                        if isinstance(values, list):
                            for value in content.get(key):
                                cw.write("  * %s\n" % value)
                        cw.write("\n")

                cw.write("\n")
                cw.write(" -- %s <%s> %s\n" % (get_builder_name(), get_builder_email(), get_changelog_time()))
            os.rename(os.path.join(self.debian_path, 'debian', 'changelog.dch'), os.path.join(self.debian_path, 'debian', 'changelog'))
        except Exception as err:
            print(err)
            return False

        return True


class GitBuild():
    def __init__(self, cache_path, pkgname, source, debian): 
        '''
        source and debian shoule be a dict like: {'url': url, 'branch': 'master', 'hashsum': 'HEAD', 'force': True}
        '''
        self.cache_path = cache_path
        self.pkgname = pkgname
        self.source = source
        self.debian = debian

    def execute(self, subprocess_args, cwd=None, extended_output=False, timeout=600):
        if cwd is None:
            cwd = os.path.join(self.cache_path, self.pkgname)
        env = os.environ.copy()
        env["LC_MESSAGE"] = "C"
        if isinstance(subprocess_args, str):
            subprocess_args = subprocess_args.split()
        proc = subprocess.Popen(subprocess_args, env=env, cwd=cwd, stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=None)
    
        timer = Timer(timeout, proc.kill)

        timer.start()
        try:
            stdout, stderr = proc.communicate()
            status = proc.returncode
            if stdout.endswith('\n'):
                stdout = stdout[:-1]
            if stderr.endswith('\n'):
                stderr = stderr[:-1]
        finally:
            proc.stdout.close()
            proc.stderr.close()
    
        if timer.is_alive():
            timer.cancel()
    
        if status != 0:
            print("Command: %s\n" % " ".join(subprocess_args))
            print("Error: %s\n" % stderr)
            raise subprocess.CalledProcessError("Run \"%s\" with status: %d. Error: %s" % (" ".join(subprocess_args), status, cwd))
    
        if extended_output:
            return (status, stdout, stderr)
        else:
            return stdout

    def _git_cache(self, url, path):
        if not os.path.exists(self.cache_path):
            os.makedirs(self.cache_path)

        cache = os.path.join(self.cache_path, path)
        clone = True

        if os.path.exists(cache):
            _url = self.execute(['git', 'remote', 'get-url', 'origin'], cwd=cache)
            if _url == url:
                clone = False
            else:
                print('W: source cache is not equal with config.')
                print('W: we will remove cache')
                shutil.rmtree(cache)

        if not clone:
            self.execute(['git', 'fetch', 'origin', '+refs/heads/*', '--prune'], cwd=cache)
        else:
            self.execute(['git', 'clone', '--bare', self.source['url'], self.pkgname], cwd=os.path.dirname(cache))

    def refresh_cache(self):
        '''
        initial git repo when not exists or update it 
        '''
        self._git_cache(self.source['url'], self.pkgname)

        if self.debian is not None:
            if self.debian[url] != self.source['url']:
                print('I: we will update debian.')
                debian_path = os.path.join(self.pkgname, 'debian')
                self._git_cache(self.debian['url'], debian_path)
        else:
            self.debian = self.source
            print('W: same debian and source url, we will skip debian update.')

    def get_release_version(self, ref, cwd, fakeversion=False):
        try:
            commit = self.execute([ 'git', 'rev-parse', ref ], cwd=cwd)
        except Exception as e:
            print("E: %s can not parse" % e)
            return None

        try:
            stdout = self.execute([ 'git', 'describe', '--tags', '--long', commit ], cwd=cwd)
            [ver, rev, sha ] = stdout.split('-')[:3]
            sha = sha[1:]
        except Exception as e:
            print("W: get release version error. %s" % e)
            ver = "0.0"
            rev = self.execute(['git', 'rev-list', '--count', commit ], cwd=cwd)
            sha = self.execute(['git', 'rev-parse', '--short', commit ], cwd=cwd)

        #Sometimes we will need a fakeversion
        if fakeversion:
            try:
                lastest = self.execute(['git', 'rev-list', '--tags', '--max-count=1'])
                fakever = self.execute(['git', 'describe', '--tags', lastest])
                ver = "%s.is.%s" % (fakever, ver)
            except Exception as e:
                print("W: failed to get fakever")

        return {'ver': ver, 'rev': rev, 'sha': sha}

    def get_log(self, sha, maxline=1):
        logs = self.execute(['git', 'log', "--pretty='%s'", sha, "-%d" % maxline])
        return logs.split('\n')

    def _archive(self, temp_dir, dist="experimental", urgency="low", fakeversion=False, max_changes=5):
        source_path = os.path.join(self.cache_path, self.pkgname)
        pkgver = self.get_release_version(ref=self.source['ref'], cwd=source_path, fakeversion=fakeversion)

        _revision = pkgver['sha']
        _pkgver = "%(ver)s+r%(rev)s~%(sha)s" % pkgver
        _prefix = "%s-%s/" % (self.pkgname, _pkgver)

        with open(os.devnull, 'w') as devnul:
            subprocess.check_call("git archive --format=tar --prefix=%s %s | (cd %s && tar xvf -)" % (_prefix, _revision, temp_dir), shell=True, cwd = source_path, stdout=devnull, stderr=devnull)

        if self.debian['url'] == self.source['url']:
            debian_path = source_path
        else:
            debian_path = os.path.join(self.cache_path, 'debian', self.pkgname)

        debian = self.get_release_version(ref=self.debian['ref'], cwd=debian_path)
        with open(os.devnull, 'w') as devnull:
            subprocess.check_call("git archive --format=tar --prefix=debian/ %s | (cd %s && tar xvf -)" % (debian['sha'], temp_dir), shell=True, cwd = debian_path, stdout=devnull, stderr=devnull)

        if not os.path.join(temp_dir, 'debian', 'debian'):
            raise IOError("debian path is not exists")

        subprocess.check_call("rm -rf %s" % (os.path.join(temp_dir, _prefix, 'debian')), shell=True)
        subprocess.check_call("cp -r %s %s" % (os.path.join(temp_dir, 'debian', 'debian'), os.path.join(temp_dir, _prefix)), shell=True)

        work_dir = os.path.join(temp_dir, _prefix)

        if not os.path.exists(os.path.join(work_dir, 'debian', 'source')):
            os.makedirs(os.path.join(work_dir, 'debian', 'source'))

        with open(os.path.join(work_dir, 'debian', 'source', 'format'), 'w') as fp:
            fp.write("3.0 (native)\n")

        content = self.get_log(_revision, max_changes)

        dch = Dch(work_dir)
        cp = {'Source': self.pkgname, 'Version': _pkgver, 'Distribution': dist, 'Urgency': urgency}
        dch.mangle_changelog(cp=cp, revision=_revision, content=content)
        subprocess.check_call('dpkg-source -b %s' % _prefix, shell=True, cwd=temp_dir)
        files = []
        for d in os.listdir(temp_dir):
            if os.path.isfile(os.path.join(temp_dir, d)):
                files.append(d)

        return {'pkgver':_pkgver, 'revision': _revision, 'path': temp_dir, 'files': files}

    def archive(self, ref, urgency="low", max_changes=5):
        self.refresh_cache()
        temp_prefix = os.path.join("/tmp", "git-archive-temp")
        if not os.path.exists(temp_prefix):
            os.makedirs(temp_prefix)

        temp_dir = tempfile.mkdtemp(dir=temp_prefix)
        try:
            result = self._archive(temp_dir, urgency=urgency, fakeversion=fakeversion, max_changes=max_changes)
        except Exception as e:
            subprocess.check_call("rm -rf %s" % temp_dir, shell=True)
            result = {'pkgver': None, 'message': str(e)}

        return result

    def _tag(self, temp_dir, tagver, binnum, dist="unstable", urgency="low"):
        source_path = os.path.join(self.cache_path, self.pkgname)
        _revision = self.get_release_version(ref=tagver, cwd=source_path, fakeversion=fakeversion)['sha']
        with open(os.devnull, 'w') as devnull:
            command = "git archive --format=tar --prefix=%(source)s-%(tagver)s/ %(tagver)s | xz > %(temp_dir)s/%(source)s_%(tagver)s.orig.tar.xz" % {'source': self.pkgname, 'tagver': tagver, 'temp_dir': temp_dir}
            subprocess.check_call(command, shell=True, cwd=source_path, stdout=devnull, stderr=devnull)
            subprocess.check_call("tar xf %(source)s_%(tagver)s.orig.tar.xz" % {'source': self.pkgname, 'tarver': tagver}, shell=True, cwd=temp_dir)

        work_dir = os.path.join(temp_dir, "%s-%s" % (self.pkgname, tagver))
        if self.debian['url'] == self.source['url']:
                debian_path = source_path
        else:
            debian_path = os.path.join(self.cache_path, 'debian', self.pkgname)

        with open(os.devnull, 'w') as devnull:
            debian_revision = self.get_release_version(ref=self.debian['ref'], cwd=debian_path)['sha']
            subprocess.check_call("git archive --format=tar --prefix=debian/ %s | (cd %s && tar xv -)" % (debian_revision, temp_dir), shell=True, cwd=debian_path, stdout=devnull, stderr=devnull)
            _debian_dir = os.path.join(temp_dir, 'debian', 'debian')
            if not os.path.exists(_debian_dir):
                raise IOError("debian path is not exists")

            subprocess.check_call("rm -rf %s" % (os.path.join(work_dir, 'debian')), shell=True)
            subprocess.check_call("cp -r %s %s" % (_debian_dir, work_dir), shell=True)

        if not os.path.exists(os.path.join(work_dir, 'debian', 'source')):
            os.makedirs(os.path.join(work_dir, 'debian', 'source'))

        with open(os.path.join(work_dir, 'debian', 'source', 'format'), 'w') as fp:
            fp.write("3.0 (quilt)\n")

        contents = ['Auto Version update.']
        try:
            stdout = self.execute(['git', 'cat-file', 'tag', tagver], cwd=source_path)
            _flag = False
            for line in stdout:
                if line == "":
                    add = True
                    continue
                if add:
                    contents.append(line)
        except Exception as e:
            print("W: Get tag comments error. %s" % e)
            pass

        version = tagver + '-%d' % binnum
        with open(os.path.join(work_dir, 'debian', 'changelog.dch'), 'w') as cw:
            cp = {'Source': self.pkgname, 'Version': version, 'Distribution': dist, 'Urgency': urgency}
            cw.write("%(Source)s (%(Version)s) %(Distribution)s; urgency=%(Urgency)s\n" % cp)
            cw.write("\n")
            for line in contents:
                cw.write("  * %s\n" % line)
            cw.write("\n")
            cw.write(" -- %s <%s> %s\n" % (get_builder_name(), get_builder_email(), get_changelog_time()))

        os.rename(os.path.join(work_dir, 'debian', 'changelog.dch'), os.path.join(work_dir, 'debian', 'changelog'))

        subprocess.check_call("dpkg-source -b %s" % work_dir, shell=True, cwd=temp_dir)
        files = []
        for d in os.listdir(temp_dir):
            if os.path.isfile(os.path.join(temp_dir, d)):
                files.append(d)

        return {'pkgver':version, 'revision': _revision, 'path': temp_dir, 'files': files}

    def tag(self, tagver=None, binnum=1, dist="unstable", urgency="low"):
        self.refresh_cache()
        temp_prefix = os.path.join("/tmp", "tag-archive-temp")
        if not os.path.exists(temp_prefix):
            os.makedirs(temp_prefix)

        if tagver is None:
            tagver = self.get_release_version(ref=self.source['ref'], cwd=os.path.join(self.cache_path, self.pkgname))['ver']

        temp_dir = tempfile.mkdtemp(dir=temp_prefix)
        try:
            result = self._tag(temp_dir=temp_dir, tagver=tagver, binnum=binnum, dist=dist, urgency=urgency)
        except Exception as e:
            subprocess.check_call("rm -rf %s" % temp_dir, shell=True)
            result = {'pkgver': None, 'message': str(e)}

        return result

if __name__ == "__main__":
    pass
