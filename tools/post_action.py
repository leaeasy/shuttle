import httplib2
from datetime import datetime
import json
import sys
import argparse

if len(sys.argv) == 2:
    pkgname = sys.argv[1]
    sha = "HEAD"
elif len(sys.argv) == 3:
    pkgname = sys.argv[1]
    sha = sys.argv[2]

TESTDATA1 = {'pkgname': pkgname,
             'buildtime': str(datetime.now()),
             'arches': ['amd64', 'i386'],
             'reponame': 'dde-panda',
             'dist': 'unstable',
             'action': 'tag',
             'sha': sha,
             'force': '1',
             'version': '2.0.0'
            }

URL = 'http://localhost:5000/api/package'

jsondata = json.dumps(TESTDATA1)
h = httplib2.Http()
resp, content = h.request(URL,
                          'POST',
                          jsondata,
                          headers={'Content-Type': 'application/json'})
print resp
print content
