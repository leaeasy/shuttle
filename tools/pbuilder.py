#!/usr/bin/env python
# Create pbuilder build environment

import json
import subprocess
import uuid
import os
import shutil
import sys
import hashlib
from datetime import datetime

SAVE_PATH="/srv/builders"

class PBuilder():
    def  __init__(self, json_file):
        self._json  =  json_file
        self.config  =  json.load(open(self._json, "r"))

    def storeFile(self, path, dest=None):
        print("Save %s to %s" % (path, dest))
        f = open(path)
        try:
            md5 = hashlib.md5()
            for chunk in  iter(lambda: f.read(256*1024), ''):
                md5.update(chunk)
            md5sum = md5.hexdigest()
        finally:
            f.close()
        
        if dest is not None:
            dest_name = os.path.basename(dest)
        else:
            dest_name = os.path.basename(path)

        if not os.path.exists(SAVE_PATH):
            os.makedirs(SAVE_PATH)

        _save_path = os.path.join(SAVE_PATH, dest_name)

        shutil.move(path, _save_path)

        self.config['base'] = dest_name
        self.config['md5sum'] = md5sum
        self.save()
    
    def removeFile(self):
        _base = self.config.get('base', '')
        if _base:
            if os.path.exists(os.path.join(SAVE_PATH, _base)):
                os.unlink(os.path.join(SAVE_PATH, _base))

    def save(self):
        with open(self._json, "w") as fp:
            fp.write(json.dumps(self.config, indent=4))

    def create(self, force=False):
        if force is False:
            now = datetime.now().strftime("%Y%m%d")
            if self.config.get('buildtime'):
                if int(self.config['buildtime']) >= int(now):
                    print("builder already created, and very new")
                    return True
        cmd = "pbuilder --create --debootstrapopts --no-check-gpg "
        cmd += "--mirror %(mirror)s --distribution %(dist)s --architecture %(arch)s --allow-untrusted " % self.config
        _uuid = str(uuid.uuid1())
        basetgz = os.path.join("/var/cache/pbuilder", "%s.tgz" % _uuid)
        cmd += "--basetgz %s " % (basetgz)
        if self.config.get('components'):
            cmd += "--components \"%s\" " % (" ".join(self.config['components']))

        if self.config.get('extra_packages'):
            cmd += "--extrapackages \"%s\" " % ("  ".join(self.config['extra_packages']))

        if self.config.get('other_mirrors'):
            cmd += "--othermirror \"%s\" " % (" | ".join(self.config['other_mirrors']))

        state = None
        print(cmd)

        try:
            proc = subprocess.Popen(cmd, shell=True, stderr=subprocess.STDOUT, stdin=None)
        except Exception as err:
            state = 1

        while state == None:
            state = proc.poll()
        
        if state == 0:
            self.storeFile(basetgz, _uuid)
            return True
        else:
            return False

    def _reload(self):
        self.config  =  json.load(open(self._json, "r"))

    def dump(self):
        _contents = """
        Dist:     %(dist)s
        Arch:     %(arch)s
        Base:     %(base)s
        Hash:     %(md5sum)s
        """ % self.config
        return _contents


if  __name__  == "__main__":
    if len(sys.argv) == 2:
        json_file =  sys.argv[1]
        p = PBuilder(json_file=json_file)
        p.create()
