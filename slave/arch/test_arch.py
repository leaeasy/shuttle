from configparser import ConfigParser
from twisted.internet import reactor

cf = ConfigParser()
cf.read("shuttle-slave-example.conf")

from slave import BuildDSlave

slave = BuildDSlave(cf)

from archpackage import ArchBuildManager

deb = ArchBuildManager(slave, '12345')

slave.startBuild(deb)
deb.initiate(pkg_name="alsa-utils", extra_args={'arch_tag': 'i386', 'repo': 'community'})

reactor.run()
