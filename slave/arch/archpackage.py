__metaclass__ = type

import os
import re
import signal

from twisted.python import log
from slave import BuildManager

class BuildExitCodes:
    OK           =  0
    FAILED       =  1
    ATTEMPTED    =  2
    GIVENBACK    =  3
    BUILDERFAIL  =  4


class ArchBuildState:
    INIT      = "INIT"
    PREPARE   = "PREPARE"
    PREBUILD  = "PREBUILD"
    UPDATE    = "UPDATE"
    BUILD     = "BUILD"
    CLEANUP   = "CLEANUP"

class ArchBuildManager(BuildManager):

    initial_build_state = "archlinux build with makepkg"

    def __init__(self, slave, buildid, **kwargs):
        BuildManager.__init__(self, slave, buildid, **kwargs)
        self.buildid = buildid
        self._config = slave._config
        self._cachepath = slave._cachepath

        self.pkgbuild = self._config.get("slave", "pkgbuild")
        self._state = ArchBuildState.INIT
        slave.emptylog()
        self.alreadyfailed = False

    def initiate(self, pkg_name, extra_args):
        self.arch_tag = extra_args.get("arch_tag", self._slave.getArch())
        self.pkg_name = pkg_name
        self.runSubProcess(command="/bin/date", args=["date", "+%A %T"])

    def gatherResults(self):
        if not os.path.exists(self.work_path):
            return
        for entry in os.listdir(self.work_path):
            if ".pkg.tar" in entry:
                self._slave.addWaitingFile(os.path.join(self.work_path, entry))

    def iterate(self, success):
        if self.alreadyfailed and success == 0:
            success = 128 + singal.SIGKILL
        self._slave.log("Iterating with success flag %s against stage %s" % (success, self._state))
        func = getattr(self, "iterate_" + self._state, None)
        if func is None:
            raise ValueError("Unknow internal state " + self._state)
        func(success)
        
    def iterateReap(self, state):
        self._slave.log("Iterating with failed flag againest stage %s" % (self._state))
        self.alreadyfailed = True
        self._state = ArchBuildState.CLEANUP

    def doPreBuild(self):
        self.work_path = os.path.join(self.pkgbuild, self.pkg_name)
        if not os.path.exists(self.work_path):
            self.iterateReap(self._state)
        else:
            self.runSubProcess(command='/usr/bin/makepkg', args=['makepkg', '--printsrcinfo'], path=self.work_path)

    def doUpdate(self):
        self.runSubProcess(command='/usr/bin/makepkg', args=['makepkg', '-g'], path=self.work_path)

    def doBuild(self):
        self.runSubProcess(command='/usr/bin/makepkg', args=['makepkg', '--skipchecksums', '--skippgpcheck', '-f'], path=self.work_path)

    def doCleanup(self):
        self.runSubProcess(command='/bin/ls', args=['ls', '-l', self.work_path])
        self._slave.log("Finished clean up build environment")

    def iterate_INIT(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
                self._slave.buildFail()
            self._state = ArchBuildState.CLEANUP
        else:
            self._state = ArchBuildState.PREBUILD
            self.doPreBuild()

    def iterate_PREBUILD(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
            self._state = ArchBuildState.CLEANUP
        else:
            self._state = ArchBuildState.UPDATE
            self.doUpdate()

    def iterate_UPDATE(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
            self._state = ArchBuildState.CLEANUP
        else:
            self._state = ArchBuildState.BUILD
            self.doBuild()

    def iterate_BUILD(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
        else:
            self.gatherResults()
        self._state = ArchBuildState.CLEANUP
        self.doCleanup()

    def iterate_CLEANUP(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
            self._slave.buildFail()
        else:
            if not self.alreadyfailed:
                self._slave.buildOK()

        self._slave.buildComplete()
