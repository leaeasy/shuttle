from ConfigParser import ConfigParser
import os

from twisted.application import service, strports
from twisted.web import resource, server, static

from archpackage import ArchBuildManager
from slave import XMLRPCBuildDSlave

conffile = os.environ.get("SHUTTLE_SLAVE_CONFIG", "shuttle-slave-example.conf")

conf = ConfigParser()
conf.read(conffile)
slave = XMLRPCBuildDSlave(conf)

slave.registerBuilder(ArchBuildManager, "arch")

application = service.Application("BuildSlave")
builddslaveService = service.IServiceCollection(application)

root = resource.Resource()
root.putChild(b"rpc", slave)
root.putChild(b"filecache", static.File(conf.get('slave', 'filecache')))

slavesite = server.Site(root)
strports.service(slave.slave._config.get("slave", "bindport"), slavesite).setServiceParent(builddslaveService)
