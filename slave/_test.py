from ConfigParser import ConfigParser
import os
from  slave  import BuildDSlave



config = ConfigParser()
config.read('shuttle-slave-example.conf')
slave  = BuildDSlave(config)
slave.uploadpath = os.path.join('dummy', '1')
slave.storeFile("/tmp/shuttle.db")

from twisted.internet import reactor
reactor.run()
