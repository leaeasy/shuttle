Format: 3.0 (native)
Source: shuttle-debian-slave
Binary: shuttle-debian-slave
Architecture: all
Version: 0.1~1
Maintainer: Deepin Sysdev <sysdev@deepin.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9), dh-systemd
Package-List:
 shuttle-debian-slave deb devel optional arch=all
Checksums-Sha1:
 89016de0ace531bb3cdd50efbecf6364b4d1f564 10352 shuttle-debian-slave_0.1~1.tar.xz
Checksums-Sha256:
 4efdc7ac88ddb52cbeac5ae6ffa88f63ee7cddc8ad077cfc8213c6fc11528795 10352 shuttle-debian-slave_0.1~1.tar.xz
Files:
 862701fa65a5f479d1653206f0eaa5af 10352 shuttle-debian-slave_0.1~1.tar.xz
