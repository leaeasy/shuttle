from ConfigParser import ConfigParser
from twisted.internet import reactor

cf = ConfigParser()
cf.read("shuttle-slave-example.conf")

from slave import BuildDSlave

slave = BuildDSlave(cf)

from pbuilder import PbuildBuildManager

pb = PbuildBuildManager(slave, '9999')
slave.startBuild(pb)

pb.initiate(configfile="default.json", extra_args={'librarian':'http://127.0.0.1/shuttle/config/builder', 'arch':'i386', 'dist':'unstable'})

reactor.run()
