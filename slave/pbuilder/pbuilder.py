__metaclass__ = type

import os
import re
import signal
from functools import partial

from twisted.python import log
from slave import BuildManager

class BuildExitCodes:
    OK           =  0
    FAILED       =  1
    BUILDERFAIL  =  2


class BuildState:
    INIT      = "INIT"
    BUILD     = "BUILD"
    FINISH    = "FINISH"

class PbuildBuildManager(BuildManager):

    extra_info = "Pbuilder Build Manager"

    def __init__(self, slave, buildid, **kwargs):
        BuildManager.__init__(self, slave, buildid, **kwargs)
        self.buildid = str(buildid)

        self._cachepath = slave._cachepath
        self._binpath = slave._binpath

        self._state = BuildState.INIT
        slave.emptylog()
        self.alreadyfailed = False

    def initiate(self, configfile, extra_args):
        self.configfile = configfile
        self.baseurl = extra_args.get("librarian")
        self.arch = extra_args.get('arch')
        self.dist = extra_args.get('dist')
        command = os.path.join(self._binpath, "get-config")
        self.runSubProcess(command=command, args=['get-config', self.buildid, self.baseurl, self.configfile])

    def gatherResult(self):
        results = os.listdir(self._cachepath)
        for path in results:
            if path != 'buildlog':
                self._slave.addWaitingFile(path)

        if len(self._slave.waitingfiles) < 2:
            extra_info = "Gather result failed." 
            self.alreadyfailed = True
            self._slave.buildFail(extra_info)
            return

    def iterate(self, success):
        if self.alreadyfailed and success == 0:
            success = 128 + signal.SIGKILL

        log.msg("Iterating with success flag %s against stage %s" % (success, self._state))
        func = getattr(self, "iterate_" + self._state, None)
        if func is None:
            raise ValueError("Unknow internal state " + self._state)
        func(success)
        
    def doBuild(self):
        command = os.path.join(self._binpath, "build-image")
        self.runSubProcess(command=command, args=['build-image', self.buildid, self.configfile, self.dist, self.arch, self._cachepath])

    def doFinish(self):
        command = os.path.join(self._binpath, "clean-build")
        self.runSubProcess(command=command, args=['clean-build', self.buildid])

    def iterate_INIT(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
                self._slave.buildFail()
            self._state = BuildState.FINISH
            self.doFinish()
        else:
            self._state = BuildState.BUILD
            self.doBuild()

    def iterate_BUILD(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
                self._slave.buildFail()
        else:
            self.gatherResult()

        self._state = BuildState.FINISH
        self.doFinish()

    def iterate_FINISH(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
                self._slave.buildFail()
        else:
            if not self.alreadyfailed:
                self._slave.buildOK()

        self._slave.buildComplete()
