__metaclass__ = type

import os
import re
import signal
import glob

from twisted.python import log
from slave import BuildManager

from archpackage import ArchBuildState
from archpackage import BuildExitCodes

class DummyBuildManager(BuildManager):

    initial_build_state = "Dummy Builder Manager"

    def __init__(self, slave, buildid, **kwargs):
        BuildManager.__init__(self, slave, buildid, **kwargs)
        self.buildid = buildid
        self._config = slave._config

        self._cachepath = slave._cachepath
        self._state = ArchBuildState.INIT
        slave.emptylog()

        self.alreadyfailed = False

    def initiate(self,filemap, extra_args):
        self.runSubProcess(command="/bin/date", args=["date", "+%A %T"])

    def gatherResults(self):
        for path in glob.glob(os.path.join(self._cachepath, "*.db")):
            self._slave.addWaitingFile(path)

    def iterate(self, success):
        if self.alreadyfailed and success == 0:
            success = 128 + singal.SIGKILL
        self._slave.log("Iterating with success flag %s against stage %s" % (success, self._state))
        func = getattr(self, "iterate_" + self._state, None)
        if func is None:
            raise ValueError("Unknow internal state " + self._state)
        func(success)
        
    def iterateReap(self, state):
        self._slave.log("Iterating with failed flag againest stage %s" % (self._state))
        self.alreadyfailed = True
        self._state = ArchBuildState.CLEANUP

    def doPreBuild(self):
        self.runSubProcess(command='/bin/echo', args=['echo', 'Prebuilding ---', str(self.buildid)])

    def doUpdate(self):
        self.runSubProcess(command='/bin/sleep', args=['sleep', '1'])

    def doBuild(self):
        import random
        id = random.randint(0,99)
        self.runSubProcess(command='/usr/bin/install', args=['install', '-Dm755', '/tmp/shuttle.db', os.path.join(self._cachepath, str(id)+ 'result.db')])

    def doCleanup(self):
        self.runSubProcess(command='/bin/echo', args=['echo', 'Cleanning  ---'])

    def iterate_INIT(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
                self._slave.buildFail()
            self._state = ArchBuildState.CLEANUP
        else:
            self._state = ArchBuildState.PREBUILD
            self.doPreBuild()

    def iterate_PREBUILD(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
            self._state = ArchBuildState.CLEANUP
        else:
            self._state = ArchBuildState.UPDATE
            self.doUpdate()

    def iterate_UPDATE(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
            self._state = ArchBuildState.CLEANUP
        else:
            self._state = ArchBuildState.BUILD
            self.doBuild()

    def iterate_BUILD(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
        else:
            self.gatherResults()
        self._state = ArchBuildState.CLEANUP
        self.doCleanup()

    def iterate_CLEANUP(self, success):
        if success != 0:
            if not self.alreadyfailed:
                self.alreadyfailed = True
            self._slave.buildFail()
        else:
            if not self.alreadyfailed:
                self._slave.buildOK()

        self._slave.buildComplete()
