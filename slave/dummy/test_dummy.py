from ConfigParser import ConfigParser
from twisted.internet import reactor

cf = ConfigParser()
cf.read("shuttle-slave-example.conf")

from slave import BuildDSlave

slave = BuildDSlave(cf)

from dummypackage import DummyBuildManager

deb = DummyBuildManager(slave, '12345')

slave.startBuild(deb)
deb.initiate(filemap='', extra_args={'arch_tag': 'i386', 'repo': 'community'})

reactor.run()
