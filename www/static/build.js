$(document).ready(function(){
	instance_name = $("#instance_name").text();
	id = location.href.split('/').pop();
	$.post("/api/v1/"+instance_name+"/"+id, function(result){
		switch(result.status){
			case "import imported":
				$(this).addClass('info');
				break;
		};

		$.each(result.jobs, function(_, jobs) {
			for (var job in jobs) {
				$("#build-" + job).addClass("label-" + jobs[job]);
			}
		});
	});
})
