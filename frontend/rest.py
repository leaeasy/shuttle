#!/usr/bin/env python
import json

from twisted.web import resource, server
from twisted.internet import defer, threads, reactor
from twisted.python import failure
import time
import glob
import os

from shuttle import ShuttleStore, ShuttleJob
from shuttle.Shuttle import Shuttle
from shuttle.ShuttleConfig import ShuttleConfig
from shuttle.ShuttleJob import JobStatus, UploadStatus
from shuttle.ShuttleSlave import ShuttleSlaveConfig
shuttle = Shuttle()
cfg = ShuttleConfig()

class CachePackagesInfo():
    def __init__(self, name):
        self.name = name
        self.cache = []
        config = os.path.join(cfg.get('build', 'configdir'), 'package', name) 
        if not os.path.exists(config):
            config = os.path.join(cfg.get('build', 'configdir'), 'package', 'defaults') 

        json_files = glob.glob(os.path.join(config, '*.json'))
        for config in json_files:
            if os.path.exists(config):
                with open(config, "r") as fp:
                    pkgname = os.path.basename(config)[:-5]
                    info = json.loads(fp.read())
                    info.update({'name': pkgname})
                    self.cache.append(info)

class RestRootResource(resource.Resource):

    def __init__(self):
        resource.Resource.__init__(self)
        self.putChild("projects", GetProjects())
        self.putChild("package", PackageAction())
        self.putChild("job", JobAction())
        self.putChild("pkginfo", PackageInfo())

    def getChild(self, name, request):
        if name == '':
            return self
        return resource.Resource.getChild(self, name, request)

    def render_GET(self, request):
        #TODO
        return '''
        <html>
            <body>
            <a href="projects">Projects</a>
            </body>
        </html>
        '''

class GetProjects(resource.Resource):

    isLeaf = True
    repo = os.path.join(cfg.get('build', 'configdir'), 'repo')

    def render_GET(self, request):
        request.setHeader("content-type", "application/json")
        self.response = {'self': {'href': request.uri}}
        def fail(failure):
            request.setResponseCode(200)
            self.response.update({'status': 'failed', 'message': failure.getErrorMessage()})
            request.write(json.dumps(self.response))
            request.finish()

        def success(success):
            request.setResponseCode(400)
            self.response.update({'status': 'success'})
            request.write(json.dumps(self.response))
            request.finish()

        def get_projects():
            configs = glob.glob(os.path.join(self.repo, "*.json"))
            results = []
            if len(configs) == 0:
                return failure.Failure(Exception("None of project config had found"))
            for config in configs:
                with open(config, "r") as fp:
                    info = json.loads(fp.read())
                results.append(info)

            self.response['projects'] = results

        def get_project(name):
            config = os.path.join(self.repo, "%s.json" % name)
            if not os.path.exists(config):
                return failure.Failure(Exception("project %s config had found" % name))
            with open(config, "r") as fp:
                info = json.loads(fp.read())
            pkgs_cache = CachePackagesInfo(name)

            pkgs = []
            for cache in pkgs_cache.cache:
                if cache.get('ppa', None) == name:
                    pkgs.append(cache)
            info.update({'pkgs': pkgs})

            self.response['project'] = info

        if len(request.postpath) == 0:
            d = threads.deferToThread(get_projects)
        else:
            d = threads.deferToThread(get_project, request.postpath[0])

        d.addCallback(success)
        d.addErrback(fail)

        return server.NOT_DONE_YET

class PackageAction(resource.Resource):
    isLeaf = True

    def render_POST(self, request):
        content = json.loads(request.content.read())

        action = content.get('action', None)
        dist = content.get('dist', None)

        def callback(result):
            request.write(json.dumps(result))
            request.finish()

        def failure(result):
            request.write(json.dumps({'status': 'failed', 'message': result.getErrorMessage()}))
            request.finish()

        if action in ['commit', 'nightly', 'tag']:
            pkgname = content.get('pkgname', None)
            sha = content.get('sha', None)
            reponame = content.get('reponame', None)
            force = content.get('force', '0')
            arches = content.get('arches', ['amd64'])
            if pkgname is None or reponame is None or arches is None: 
                result  = {'status': 'failed', 'message': 'pkgname should not be none'}
                return json.dumps(result)
            if force == '0' or str(force).lower() == 'false':
                force = False
            else:
                force = True

            if pkgname:
                try:
                    d = threads.deferToThread(ShuttleStore.Package.add, package=pkgname, reponame=reponame, arches=arches, action=action, dist=dist, sha=sha, force=force)
                    d.addCallback(callback)
                    d.addErrback(failure)
                    return server.NOT_DONE_YET
                except Exception as e:
                    return json.dumps({'status': 'failed', 'message': e.getErrorMessage()})

    def render_GET(self, request):
        if len(request.postpath) == 0:
            return json.dumps({'status': 'ignore', 'message': 'url should spcify pkgid'})
        else:
            pkgid = request.postpath[0]
            pkgs = shuttle.get_all_packages(id=pkgid)
            if len(pkgs) != 1:
                return json.dumps({'status': 'failed', 'message': '%s is not exists' % pkgid})
            pkg = pkgs[0]
            jobs = shuttle.get_all_jobs(package=pkg)
            result = {'status': 'success'}
            jobs_info = []
            for job in jobs:
                result = json.loads(job.jsonify())
                jobs_info.append(result)
            result = {'status': 'success', 'pkgname': pkg.pkgname,
                    'pkgver': pkg.pkgver, 'reponame': pkg.reponame,
                    'hashsum': pkg.hashsum,
                    'action': pkg.action, 'jobs': jobs_info}
            return json.dumps(result)

class JobAction(resource.Resource):
    isLeaf = True
    def render_POST(self, request):
        content = json.loads(request.content.read())
        action = content.get('action', None)
        result = None
        pkgid = content.get('pkgid', None)
        if pkgid is None:
            return json.dumps({'status': 'failed', 'message': 'pkgid should not be none'})
        if ShuttleJob.Package.selectBy(id=int(pkgid)).count() == 0:
            return json.dumps({'status': 'failed', 'message': 'pkgid not found in database'})

        if action == 'rebuild':
            pkg = ShuttleJob.Package.selectBy(id=int(pkgid))[0]
            pkg.upload_status = UploadStatus.UNKNOWN
            for job in pkg.jobs:
                job.status = JobStatus.WAIT
            result = {'status': 'success', 'message': 'pkgid has requeued'}
            return json.dumps(result)

        if action == 'abort':
            pkg = ShuttleJob.Package.selectBy(id=int(pkgid))[0]
            for job in pkg.jobs:
                if job.status != JobStatus.BUILDING:
                    continue
                for slave in ShuttleSlaveConfig().slaves():
                    if job.build_host == slave.name:
                        slave.proxy.abort()
                        break
            return json.dumps({'status': 'success', 'message': 'Job has abort'})

        if action == 'cancel':
            pkg = ShuttleJob.Package.selectBy(id=int(pkgid))[0]
            for job in pkg.jobs:
                if job.status == JobStatus.WAIT:
                    job.status = JobStatus.CANCELED
            return json.dumps({'status': 'success', 'message': 'Job has canceled'})


    def render_GET(self, request):
        results = []
        if len(request.postpath) == 0:
            jobs = shuttle.get_all_jobs()
        else:
            jobid = request.postpath[0]
            jobs = shuttle.get_all_jobs(id=jobid)
        for job in jobs:
            result = json.loads(job.jsonify())
            results.append(result)
        return json.dumps({'status':'success', 'jobs': results})

class AbandonPackageAction(resource.Resource):
    isLeaf = True

    def render_GET(self, request):
        pkgname = request.args.get('package', None)
        tag = request.args.get('tag', None)
        reponame = request.args.get('reponame', None)

        if pkgname is None:
            result  = {'status': 'failed', 'message': 'pkgname should not be none'}
            return json.dumps(result)

        def callback(result):
            request.write(json.dumps(result))
            request.finish()

        def failure(result):
            request.write(json.dumps({'status': 'failed', 'message': result.getErrorMessage()}))
            request.finish()

        if tag is None:
            action='commit'
            arches=['amd64']
            if reponame is None:
                reponame = ['experimental']
        else:
            action='tag'
            arches=['amd64', 'i386']
            if reponame is None:
                reponame = ['dde']

        d = threads.deferToThread(ShuttleStore.Package.add, package=pkgname[0],reponame=reponame[0], arches=arches, action=action)
        d.addCallback(callback)
        d.addErrback(failure)
        return server.NOT_DONE_YET

class PackageInfo(resource.Resource):
    isLeaf = True

    def render_GET(self, request):
        request.setHeader("Access-Control-Allow-Origin", "*")
        request.setHeader("Access-Control-Allow-Methods", "POST,GET")  
        pkgname = None
        sha = None
        if request.args.get('pkgname', None):
            pkgname = request.args['pkgname'][0]
        if request.args.get('sha', None):
            sha = request.args['sha'][0]

        if request.args.get('tag', None):
            action = 'tag'
        else:
            action = 'commit'

        def callback(result):
            request.setHeader("content-type", "application/json")
            request.write(json.dumps(result))
            request.finish()

        def get_result(pkgname, sha, atcion):
            if pkgname is None or sha is None:
                return {'status': 'failed', 'message': 'pkgname or sha should not be none'}

            if len(sha) > 7:
                sha = sha[:7]
            if sha == '':
                pkgs = shuttle.get_all_packages(pkgname=pkgname, action=action)[:1]
            else:
                pkgs = shuttle.get_all_packages(pkgname=pkgname, hashsum=sha, action=action)
            if len(pkgs) == 0:
                return {'status': 'failed', 'message': 'failed to get pkg'}
            else:
                infos = []
                for pkg in pkgs:
                    jobs = []
                    for job in pkg.jobs:
                        jobs.append({'id': job.id, 'dist': job.dist, 'arch': job.arch, 'status': job.status}) 
                    infos.append({'id': pkg.id, 'version': pkg.pkgver, 'reponame': pkg.reponame, 'hash': pkg.hashsum,  'jobs': jobs})
                result = {'status': 'success', 'infos': infos}
                return result

        d = threads.deferToThread(get_result, pkgname, sha, action)
        d.addCallback(callback)
        return server.NOT_DONE_YET
            
if __name__ == "__main__":
    root = RestRootResource()
    site = server.Site(root)
    reactor.listenTCP(5000, site)
    reactor.run()
