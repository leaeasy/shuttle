from twisted.web import resource, server
from twisted.internet import defer, threads
from jinja2 import Environment, FileSystemLoader
from shuttle.ShuttleConfig import ShuttleConfig
from shuttle.Shuttle import Shuttle
from shuttle import ShuttleJob
from shuttle.ShuttleSlave import ShuttleSlaveConfig
from shuttle.ShuttleJob import JobStatus, PbuildStatus

from frontend.rest import CachePackagesInfo
from shuttle import ShuttleStore 

from frontend import common 

import os
import glob
import json

jinja = Environment(loader=FileSystemLoader(searchpath=ShuttleConfig().get('http', 'templates_dir')))

class GetDebianUploadLog(resource.Resource):
    isLeaf = True
    template = jinja.get_template("log.html")

    def render_GET(self, request):

        def render(result):
            request.setHeader("content-type", "text/html")
            request.write(str(result))
            request.finish()

        def get_body():
            pkg = None
            if len(request.postpath) != 1:
                logs = "pkgid should be requested"
                return self.template.render(pkg=pkg, logs=logs)
            pkgid = request.postpath[0]
            pkgs = Shuttle().get_all_packages(id=pkgid)
            if len(pkgs) == 0:
                logs = "pkgid is not found"
                return self.template.render(pkg=pkg, logs=logs)
            pkg = pkgs[0]
            log_file = os.path.join(common.DebianCache, str(pkg.id), 'log')
            if os.path.exists(log_file):
                with open(log_file, 'rb') as fp:
                    fp.seek(0, os.SEEK_END)
                    count = fp.tell()
                    fp.seek(-count, os.SEEK_END)
                    logs = fp.read(count)
            else:
                logs = "No log file found"

            return self.template.render(pkg=pkg, logs=logs)

        d = threads.deferToThread(get_body)
        d.addCallback(render)
        return server.NOT_DONE_YET

class GetDebianJob(resource.Resource):
    isLeaf = True
    template = jinja.get_template("job.html")

    def render_GET(self, request):
        if len(request.postpath) != 1:
            return
        jobid = request.postpath[0]

        def get_body():
            jobs = Shuttle().get_all_jobs(id=jobid)
            if len(jobs) != 1:
                return
            job = jobs[0]

            build_slave = None

            log = "No build log found yet!"
            if job.status == JobStatus.BUILDING:
                for slave in ShuttleSlaveConfig().slaves():
                    if job.build_host == slave.name:
                        build_slave = slave
                        break
                if build_slave:
                    status = build_slave.status()
                    if status.get('filemap', None):
                        log = "Downloading files: \n"
                        for f in status['filemap']:
                            log += " %s\n" % f
                    else:
                        log = status.get('logtail', 'Something wrong with remote build slave')

            if job.status > JobStatus.BUILDING:
                if job.status == JobStatus.GIVEUP:
                    log = "Job had been Giveup!"
                else:
                    log_path = os.path.join(common.DebianCache, str(job.package.id), '%s-%s' % (job.dist, job.arch), 'buildlog')
                    try:
                        rlog = open(log_path, 'rb')
                    except IOError as e:
                        rlog = None
                        log = "%s-%s/buildlog not exists" % (job.dist, job.arch)
                    else:
                        rlog.seek(0, os.SEEK_END)
                        count = rlog.tell()
                        rlog.seek(-count, os.SEEK_END)
                        log = rlog.read(count)
                    finally:
                        if rlog is not None:
                            rlog.close()
            return str(self.template.render(job=job, log=log))

        def render(result):
            request.setHeader("content-type", "text/html")
            request.write(result)
            request.finish()


        d = threads.deferToThread(get_body)
        d.addCallback(render)
        return server.NOT_DONE_YET


class GetDebianPackages(resource.Resource):
    ifLeaf = True
    template = jinja.get_template("debian-packages.html")

    def render_GET(self, request):
        limit = 30
        builds = {}
        extra_args = {}
        if 'reponame' in request.args:
            repo = request.args.get('reponame')[0]
            extra_args['reponame'] = repo
        if 'pkgname' in request.args:
            pkgname = request.args.get('pkgname')[0] 
            extra_args['pkgname'] = pkgname
        if 'pkgver' in request.args:
            pkgver = request.args.get('pkgver')[0]
            #FIXME: '+' looks like blank in url
            pkgver = pkgver.replace(' ', '+')
            extra_args['pkgver'] = pkgver
        if 'action' in request.args:
            action = request.args.get('action')[0]
            extra_args['action'] = action

        if 'page' in request.args:
            page = int(request.args.get('page')[0])
        else:
            page = 1

        def get_body():
            total = ShuttleJob.Package.selectBy(**extra_args).count()
            pagination = total / float(limit)
            if pagination == int(pagination):
                pagination = int(pagination)
            else:
                pagination = int(pagination) + 1

            pages = list(range(1, pagination + 1))[0:10]
            pkgs = Shuttle().get_all_packages(**extra_args)[(page - 1)*limit : page*limit]
            for pkg in pkgs:
                jobs = Shuttle().get_all_jobs(packageID=pkg.id)
                builds[pkg.id]  = jobs
            status = Shuttle().status()
            return str(self.template.render(status=status, pkgs=pkgs, builds=builds, jobs=Shuttle().jobs, pages=pages, projects=common.Projects, extra_args=extra_args))

        def render(result):
            request.setHeader("content-type", "text/html")
            request.write(result)
            request.finish()

        d = threads.deferToThread(get_body)
        d.addCallback(render)
        return server.NOT_DONE_YET

class GetSlaves(resource.Resource):
    isLeaf = True
    template = jinja.get_template("slaves.html")

    def render_GET(self, request):
        def get_slaves():
            result  = []
            for slave in ShuttleSlaveConfig().slaves():
                info = {'name': slave.name, 'builders': slave.builders, 'url': slave.builder_url}
                info.update(slave.status())
                result.append(info)
            return result

        def render(result):
            request.setHeader("content-type", "text/html")
            request.write(str(self.template.render(slaves=result)))
            request.finish()

        d = threads.deferToThread(get_slaves)
        d.addCallback(render)

        return server.NOT_DONE_YET

class GetProjects(resource.Resource):
    isLeaf = True
    repo = os.path.join(ShuttleConfig().get('build','configdir'), 'repo')

    def render_GET(self, request):
        
        def get_projects():
            configs = glob.glob(os.path.join(self.repo, "*.json"))
            results = []
            for config in configs:
                with open(config, "r") as fp:
                    info = json.loads(fp.read())
                    repo_name = info.get('name')
                results.append(info)
            template = jinja.get_template("projects.html")
            return str(template.render(projects=results))

        def get_project(name, child):
            config = os.path.join(self.repo, "%s.json" % name)
            if not os.path.exists(config):
                return 
            with open(config, "r") as fp:
                info = json.loads(fp.read())

            info.update({'repos_url': ShuttleConfig().get('http', 'repos_url')})

            pkgs = []
            pkgs_cache = CachePackagesInfo(name)
            for cache in pkgs_cache.cache:
                pkgs.append(cache)

            if child == 'builds':
                template = jinja.get_template("project/builds.html")
                pkgs = Shuttle().get_all_packages(reponame=name)[:30]
                builds = {}
                for pkg in pkgs:
                    jobs = Shuttle().get_all_jobs(packageID=pkg.id)
                    builds[pkg.id]  = jobs
                return str(template.render(project=info, pkgs=pkgs, builds=builds))
            elif child == 'packages':
                template = jinja.get_template("project/packages.html")
                _pkgs = []
                for pkg in pkgs:
                    _pkgs.append(pkg['name'])
                result = ShuttleStore.Repo(name).packages(pkgs=_pkgs)
                return str(template.render(project=info, pkgs=result))

            else:
                template = jinja.get_template("project/overview.html")
                repo = os.path.join(ShuttleConfig().get('repos','basedir'), 'ppa', name)
                ppas = None
                if os.path.exists(repo) and os.path.isdir(repo):
                    ppas = os.listdir(repo)

                return str(template.render(project=info, pkgs=pkgs, ppas=ppas))

        def render(result):
            request.setHeader("content-type", "text/html")
            request.write(result)
            request.finish()


        if len(request.postpath) == 0:
            d = threads.deferToThread(get_projects)
            d.addCallback(render)
        else:
            name = request.postpath[0]
            if len(request.postpath) == 1:
                child = "overview"
            else:
                child = request.postpath[1]
            d = threads.deferToThread(get_project, name, child)
            d.addCallback(render)

        return server.NOT_DONE_YET

class GetPbuilderJobs(resource.Resource):
    isLeaf = True

    def render_GET(self, request):
        limit = 30

        def get_jobs():
            extra_args = {}
            if 'jobname' in request.args:
                jobname = request.args.get('jobname')[0]
                extra_args['jobname'] = jobname

            if 'page' in request.args:
                page = int(request.args.get('page')[0])
            else:
                page = 1
            template = jinja.get_template("pbuilder/jobs.html")
            total = ShuttleJob.Pbuild.selectBy(**extra_args).count()
            pagination = total / float(limit)
            if pagination == int(pagination):
                pagination = int(pagination)
            else:
                pagination = int(pagination) + 1

            pages = list(range(1, pagination + 1))[0:10]
            jobs = ShuttleJob.Pbuild.selectBy(**extra_args).orderBy("-id")

            return str(template.render(jobs=jobs, pages=pages))

        def get_job(jobid):
            template = jinja.get_template("pbuilder/job.html")
            job = ShuttleJob.Pbuild.selectBy(id=jobid)[0]
            log = common.get_job_log(jobid)
            return str(template.render(job=job, log=log))

        def failed(failure):
            request.write("failed to access: %s" % failure)
            request.finish()

        def render(result):
            request.setHeader("content-type", "text/html")
            request.write(result)
            request.finish()

        if len(request.postpath) == 0:
            d = threads.deferToThread(get_jobs)
        else:
            jobid = request.postpath[0]
            d = threads.deferToThread(get_job, jobid)

        d.addCallback(render)
        d.addErrback(failed)
        return server.NOT_DONE_YET
