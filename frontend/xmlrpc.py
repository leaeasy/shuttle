from twisted.web import xmlrpc
import json
from shuttle.ShuttleJob import JobStatus
from shuttle.Shuttle import Shuttle


class XMLRPCJob(xmlrpc.XMLRPC):

    def __init__(self):
        xmlrpc.XMLRPC.__init__(self, allowNone=True)

    def xmlrpc_jobs(self, jobid):
        jobs = Shuttle().get_all_jobs(id=jobid)
        if len(jobs) == 0:
            return json.dumps({
                'status': 'failed',
                'message': 'jobid is not exists'})
        return jobs[0].jsonify()

    def xmlrpc_update(self, jobid, value):
        jobs = Shuttle().get_all_jobs(id=jobid)
        if len(jobs) == 0:
            return json.dumps({
                'status': 'failed',
                'message': 'jobid is not exists'})
        status = JobStatus.whatis(value)
        job = jobs[0]
        job.status = value
        return json.dumps({
            'status': 'success',
            'mesage': 'status changed to %s' % status})

    def xmlrpc_running(self):
        result = []
        for job in Shuttle().jobs:
            result.append(job.jsonify())
        return json.dumps({'status': 'success', 'jobs': info})
