from shuttle import ShuttleJob
from shuttle.ShuttleConfig import ShuttleConfig
from shuttle.ShuttleJob import JobStatus, PbuildStatus

import os
import glob
import json

def get_projects():
    repo = os.path.join(ShuttleConfig().get('build','configdir'), 'repo')
    configs = glob.glob(os.path.join(repo, "*.json"))
    projects = []
    for config in configs:
        with open(config, "r") as fp:
            info = json.loads(fp.read())
            if info.get('hidden', '0') != '1':
                projects.append(info)
    return projects


Projects = get_projects()
DebianCache = os.path.join(ShuttleConfig().get('build', 'cachedir'), 'debian-package')

def get_job_log(jobid):
    job = ShuttleJob.Pbuild.selectBy(id=jobid)[0]
    log = "No build log found yet!"
    if job.status > PbuildStatus.BUILDING:
        cache = os.path.join(ShuttleConfig().get('build', 'cachedir'), 'pbuilder')
        log_path = os.path.join(cache, str(job.id), 'buildlog')
        if os.path.exists(log_path):
            try:
                rlog = open(log_path, 'rb')
            except IOError as e:
                rlog = None
                log = "buildlog not exists" % (job.dist, job.arch)
            else:
                rlog.seek(0, os.SEEK_END)
                count = rlog.tell()
                rlog.seek(-count, os.SEEK_END)
                log = rlog.read(count)
            finally:
                if rlog is not None:
                    rlog.close()
    return log
