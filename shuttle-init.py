from shuttle import ShuttleJob
import sqlobject
from shuttle.ShuttleConfig import ShuttleConfig
import os
import sys

if not os.path.exists(cfg.config_file):
    cfg = ShuttleConfig(dontparse=True)
    print("W: no config file exists, generate it first...")
    cfg.save()
    print("I: save config file - %s, please modify it and run init again" % cfg.config_file) 
    sys.exit(0)

cfg = ShuttleConfig()
conn = sqlobject.connectionForURI(cfg.get("build", "database_uri"))
sqlobject.sqlhub.processConnection = conn

ShuttleJob.Package.createTable(ifNotExists=True)
ShuttleJob.Job.createTable(ifNotExists=True)
ShuttleJob.Pbuild.createTable(ifNotExists=True)

for d in [ cfg.get('build', 'cachedir'), cfg.get('build', 'basetgz'), cfg.get('repos', 'basedir')]:
    if not os.path.exists(d):
        os.makedirs(d)

from shuttle.ShuttleReprepro import Reprepro
import glob
for r in glob.glob(os.path.join(cfg.get('repos', 'config'), '*.json')):
    reponame = os.path.basename(r)[:-5]
    Reprepro(reponame).create_config()

