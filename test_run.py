from shuttle import ShuttleJob
from shuttle.ShuttleJob import PbuildStatus
from shuttle.Shuttle import Shuttle
from shuttle.ShuttleConfig import ShuttleConfig
import os

Shuttle()

cachedir = ShuttleConfig().get("build", "cachedir")

def upload_done(jobid):
    job = ShuttleJob.Pbuild.selectBy(id=jobid)[0]
    if job.status == PbuildStatus.BUILD_OK:
        new_path = os.path.join(cachedir, 'pbuilder', str(jobid))
        linkname = "%s.%s.%s" % (job.jobname, job.dist, job.arch)
        linkpath = os.path.join(ShuttleConfig().get("build", "basetgz"), job.field, linkname)

        if not os.path.exists(os.path.dirname(linkpath)):
            os.makedirs(os.path.dirname(linkpath))

        if os.path.exists(linkpath) and os.path.islink(linkpath):
            old_path = os.path.realpath(linkpath)
        else:
            old_path = None
        print(linkpath, new_path, old_path)
        os.system("rm -f %s" % linkpath)
        os.system("ln -sf %s %s" % (new_path, linkpath))

        if old_path is not None:
            item = os.path.join(old_path, "base.tgz")
            os.system("rm -f %s" % item)

upload_done(5)
