#!/usr/bin/env python
from shuttle.Shuttle import Shuttle
from twisted.internet.task import LoopingCall
from twisted.internet import reactor
from shuttle.ShuttleReprepro import ShuttleRepo

from shuttle.ShuttleStore import LoopAddPackage

from crontab import CronTab
morning = CronTab("0 4 * * *")

from shuttle.ShuttleJob import PbuildStatus
from shuttle import ShuttleJob
from shuttle.ShuttlePbuild import ShuttlePbuilder

def pbuilder_nightly():
    ShuttleJob.Pbuild(field="debian", jobname="default", dist="unstable", arch="amd64", status=PbuildStatus.WAIT)
    for baseimg in ['default', 'qt5', 'go']:
        for arch in ['amd64', 'i386']:
            ShuttleJob.Pbuild(field="deepin", jobname=baseimg, dist="unstable", arch=arch, status=PbuildStatus.WAIT)

shuttle =Shuttle()
#pbuilder_nightly()

addpackage = LoopingCall(LoopAddPackage('debian').loop, True, ['amd64'])
addpackage.start(10)

#reactor.callLater(morning.next(), pbuilder_nightly)

reactor.run()
