# config/slave.conf
[debian-builder-01]
method = debian-package
rpc = http://localhost:port

[pbuilder]
method = pbuilder-image
rpc = http://localhost:port

# repo/{{NAME}}.json
{
    "name": "dde",
    "base": "deepin",
    "archives": [
	"deb [trusted=yes] http://pools.corp.deepin.com/deepin distribution main contrib"
    ],
    "description": "package source developed by ci.deepin.org",
    "dists": ["unstable"],
    "arches": ["amd64", "i386" ],
    "url": "http://10.0.10.32/shuttle/repos/dde/",
    "experimental-arches": ["amd64"],
    "skip_source": "0",
    "gpg_check": "0",
    "gpg_sign": "0"
}

# builder/{{NAME}}/default.json

{
    "name": "default",
    "type": "tarball", 
    "components": [
        "main", 
        "contrib", 
        "non-free"
    ], 
    "extra_packages": [
	"gnupg",
	"deepin-keyring"
    ],
    "mirror": "http://pools.corp.deepin.com/deepin"
}

# package/{{NAME}}/{{pkgname}}.json
Please see https://cr.deepin.io/shutter

