#!/usr/bin/env python
from twisted.web import resource, util
from twisted.internet import  reactor
from twisted.web.server import Site
from twisted.web.static import File
from twisted.web.util import redirectTo
from shuttle.ShuttleConfig import ShuttleConfig
from shuttle.Shuttle import Shuttle

import sys
reload(sys)
sys.setdefaultencoding('utf8')

from frontend import html, rest, xmlrpc

shuttle = Shuttle()

class DebianResource(resource.Resource):
    def __init__(self):
        resource.Resource.__init__(self)
        self.putChild("packages", html.GetDebianPackages())
        self.putChild("job", html.GetDebianJob())
        self.putChild("upload", html.GetDebianUploadLog())

class RootResource(resource.Resource):
    def __init__(self):
        resource.Resource.__init__(self)
        self.putChild("static", File(ShuttleConfig().get('http', 'statics_dir')))
        self.putChild("filecache", File(ShuttleConfig().get('build', 'cachedir')))

        self.putChild("projects", html.GetProjects())
        self.putChild("pbuilder", html.GetPbuilderJobs())

        self.putChild("slaves", html.GetSlaves())
        self.putChild("debian", DebianResource())

        self.putChild("api", rest.RestRootResource())
        self.putChild("rpc", xmlrpc.XMLRPCJob())
        self.putChild("build", rest.AbandonPackageAction())
        self.putChild("", util.Redirect("/debian/packages"))

if __name__ == "__main__":
    root = RootResource()
    factory = Site(root)
    reactor.listenTCP(5000, factory)
    reactor.run()
