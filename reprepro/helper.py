#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import fcntl
import subprocess


class LockContext:
    def __init__(self, lockfilename = None):
        if lockfilename:
            self.lockfilename = lockfilename
        else:
            self.lockfilename = '/tmp/reprepro_check.lock'

    def __enter__(self):
        self.lfp = open(self.lockfilename, 'w')
        try:
            fcntl.lockf(self.lfp, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError as err:
            raise IOError("Could lock file %s: %s" % (self.lockfilename, err))

        return self


    def __exit__(self, exception_type, exception_val, trace):
        self.lfp.close()
        return False

def try_run_command(command):
    try:
        subprocess.check_call(command)
        return True
    except Exception as err:
        print("Execution of [%s] Failed:%s" % (command, err))
        return False

def delete_unreferenced(repo_path, conf_path):
    clean_up_command = [ 'reprepro', '-v' , '-b', repo_path, '-c', conf_path, 'deleteunreferenced' ]
    return try_run_command(clean_up_command)

def invalidate_package(repo_path, conf_path, distro, arch, package):
    """ Remove this package itself from repo"""
    debtype = 'deb' if arch != 'source' else 'dsc'
    arch_match = ',Architecture (== ' + arch + ' )' if arch != 'source' else ''

    invalidate_package_command = [ 'reprepro', '-b', repo_path, '-c', conf_path,
            '-T', debtype, '-V', 'removefilter', distro, 
            "package (==" + package + " )" + arch_match ]

    return try_run_command(invalidate_package_command)

def invalidate_dependent(repo_path, conf_path, distro, arch, package):
    """ Remove all dependencies of the package with the same arch. """
    invalidate_dependent_command = [ 'reprepro', '-V', '-b', repo_path, '-c', conf_path,
            '-T', 'deb', 'removefilter', distro, 
            "Architecture (== " + arch + " )," + 
            "( Depends (% *" + package + "[, ]* )" +
            "| Depends (% *" + package + ") )" ]
    return try_run_command(invalidate_dependent_command)

def update_repo(repo_path, conf_path, dist_generator, updates_generator, distro, arch, commit=False):
    command_argument = 'update' if command else 'dumpupdate'
    update_command = [ 'reprepro', '-v', '-b', repo_path, '--noskipold', command_argument, distro ]

    lockfile = os.path.join(repo_path, 'lock')
    update_filename = os.path.join(conf_path, 'updates')
    distribution_filename = os.path.join(conf_path, 'distributions')

    with LockContext(lockfile) as lock_c:
        print("Creating updates file %s" % update_filename)
        with open(update_filename, 'w') as fp:
            fp.write(updates_generator.generate_file_contents(distro, arch))

        print("Creating distributions file %s" % distribution_filename)
        with open(distribution_filename, 'w') as fp:
            fp.write(dist_generator.generate_file_contents(distro,arch))

        print("Running command: %s " % ' '.join(update_command))
        return try_run_command(update_command)

def add_package(repo_path, conf_path,  dist, changes):
    _command = [ 'reprepro', '-v', '-b', repo_path, '-c', conf_path, '--noskipold', '-Pnormal', '--ignore=wrongdistribution', 'include', distro, changes ]
    lockfile = os.path.join(repo_path, 'lock')
    with LockContext(lockfile) as lock_c:
        return try_run_command(_command)

def remove_package(repo_path, conf_path, dist, package):
    _command = [ 'reprepro', '-v', '-b', repo_path, '-c', conf_path, 'remove', dist, package ]
    lockfile = os.path.join(repo_path, 'lock')
    with LockContext(lockfile) as lock_c:
        return try_run_command(_command)
