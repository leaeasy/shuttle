#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

import os

from debian.deb822 import Changes

class ChangesFiles:
    def __init__(self, filename):
        try:
            self.filename = filename
            self.content = Changes(open(filename))
            self.folder = os.path.dirname(filename)
        except Exception as err:
            raise Exception("Failed to load %s: %s" % (filename, err))


def find_changes_files(folder):
    changesfiles = []
    for f in os.listdir(folder):
        if f.endswith('.changes'):
            changesfiles.append(os.path.join(folder, f))
    return changesfiles

def load_changes_files(change_files):
    changes = []
    for f in changesfiles:
        changes.append(ChangesFiles(f))
    return changes
